from datetime import date, datetime
from unittest import TestCase

from orkg import OID, ORKG, Hosts
from tests import Fabricator, mock_test


class TestTemplates(TestCase):
    orkg = ORKG(Hosts.INCUBATING)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    def test_materialize(self, *args):
        self.orkg.templates.materialize_templates(
            templates=["R199091", "R199040"], verbose=False
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_df_template(self, *args):
        from pandas import DataFrame as df

        lst = ["this", "is", "fancy"]
        lst2 = [4, 2, 5]
        param = df(list(zip(lst, lst2)), columns=["word", "length"])
        self.orkg.templates.materialize_template(template_id="R288002")
        self.orkg.templates.test_df(
            label="what!", dataset=(param, "Fancy Table"), uses_library="pyORKG"
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_optional_param(self, *args):
        self.orkg.templates.materialize_template(template_id="R275249")
        self.orkg.templates.optional_param(label="what!", uses="pyORKG")
        self.orkg.templates.optional_param(
            label="wow!", uses="pyORKG", result="https://google.com"
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_recursive_templates(self, *args):
        self.orkg.templates.materialize_template(template_id="R48000")
        self.orkg.templates.problem(
            label="Test 1",
            sub_problem=self.orkg.templates.problem(
                label="Test 2",
                sub_problem=OID("R70197"),
                same_as="https://dumpy.url.again",
                description="This is a nested test",
            ),
            same_as="https://dumpy.url",
            description="This is a test",
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_cardinality_checks(self, *args):
        self.orkg.templates.materialize_template(template_id="R48000")
        self.orkg.templates.problem(
            label="Test 2",
            sub_problem=self.orkg.templates.problem(
                label="Test 2",
                sub_problem=OID("R70197"),
                same_as="https://dumpy.url.again",
                description=["More text also!!"],
            ),
            same_as="https://dumpy.url.again",
            description=["This is a nested test"],
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_with_list_values(self, *args):
        self.orkg.templates.materialize_template(template_id="R281240")
        self.orkg.templates.multi_param(
            label="multi_param",
            uses=["pyORKG", "rORKG"],
            description="This is a nested test",
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_datetime(self, *args):
        self.orkg.templates.materialize_template("R830127")  # Air Quality Measurement
        tp = self.orkg.templates
        tp.air_quality_measurement(
            label="Observation 1",
            occurs_in=OID("wikidata:Q16"),
            stringencyindex=48.85,
            measurement_platform="Ground-based",
            method="Accounting for Effects of Meteorology, Atmospheric Chemistry, and Long-term Trends",
            has_time=tp.time_interval(has_beginning=date.today(), has_end=date.today()),
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_cardinality_issue_verena(self, *args):
        self.orkg.templates.materialize_template("R830127")
        tp = self.orkg.templates
        # create a list of measured observations
        observations = []
        for i in range(5):
            o = tp.sosaobservation(
                label="example label" + str(i),
                has_result=tp.airqualityresult(
                    label="result" + str(i),
                    has_lockdown_value=tp.quantity_value(
                        qudtunit="miligram per cubic meter",
                        qudtnumericvalue=5,
                    ),
                    has_reference_value=tp.quantity_value(
                        qudtunit="miligram per cubic meter",
                        qudtnumericvalue=5.5,
                    ),
                    has_relative_change=tp.quantity_value(
                        qudtunit="percent",
                        qudtnumericvalue="10",
                    ),
                ),
                observed_property=OID("R147322"),  # concentration
                has_feature_of_interest="Nitrogen Dioxide",
            )
            # append new template object to list
            observations.append(o)

        # create a contribution that contains all observations made for a specific location
        tp.air_quality_measurement(
            label="Paris",
            occurs_in=tp.measurement_location(
                label="Location",
                city=OID("wikidata:Q34404"),  # Paris
                country=OID("R27555"),  # France
                geographical_region=OID("R49274"),  # Europe
                additional_information="urban",
            ),
            stringencyindex=30.9,
            measurement_platform="satellites",
            method="Direct Comparison to a Reference Period",
            has_observations=observations,  # -----> this does not work
            # has_observation = observations[0] -----> this works just fine
            has_time=tp.time_interval(
                has_beginning=datetime.strptime("2020-06-08", "%Y-%m-%d").date(),
                has_end=datetime.strptime("2020-06-10", "%Y-%m-%d").date(),
            ),
        )
        self.assertTrue(True)

    @mock_test(orkg.templates.materialize_templates, return_value=Fabricator.success())
    @mock_test(orkg, "templates")
    def test_template_with_list_parameter_non_template(self, *args):
        self.orkg.templates.materialize_template(template_id="R998000")
        self.orkg.templates.multi_value_test(
            label="some label",
            description="This is a test",
            value=["pyORKG", "rORKG"],
            sample=[
                self.orkg.templates.sample_size(
                    label="sample 1", has_specified_numeric_value=5
                ),
                self.orkg.templates.sample_size(
                    label="sample 2", has_specified_numeric_value=15
                ),
            ],
        )
        self.assertTrue(True)
