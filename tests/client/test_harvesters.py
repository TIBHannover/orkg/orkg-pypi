from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestHarvesters(TestCase):
    orkg = ORKG(Hosts.INCUBATING)

    @mock_test(orkg.harvesters.doi_harvest, return_value=Fabricator.success())
    def test_doi_harvest(self, *args):
        response = self.orkg.harvesters.doi_harvest(
            doi="https://api.test.datacite.org/dois/10.7484/s06c-8y98",
            orkg_rf="Computer Sciences",
        )
        self.assertTrue(response.succeeded)

    @mock_test(orkg.harvesters.directory_harvest, return_value=Fabricator.success())
    def test_directory_harvest(self, *args):
        response = self.orkg.harvesters.directory_harvest(
            directory="/path/to/data/",
            research_field="Computer Sciences",
            title="Test paper",
            doi="https://doi.org/10.7784/x06c-3d98",
        )
        self.assertTrue(response.succeeded)
