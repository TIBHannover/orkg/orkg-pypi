from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestClasses(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.classes.by_id, return_value=Fabricator.classes.success_by_id("Paper")
    )
    def test_by_id(self, *args):
        res = self.orkg.classes.by_id("Paper")
        self.assertTrue(res.succeeded)

    @mock_test(orkg.classes.get_all, return_value=Fabricator.classes.success_get_all())
    def test_get(self, *args):
        res = self.orkg.classes.get_all()
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.classes.get_all,
        return_value=Fabricator.classes.success_get_all_by_term(q="Paper"),
    )
    def test_get_with_term(self, *args):
        res = self.orkg.classes.get_all(q="Paper")
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.classes.get_resource_by_class,
        return_value=Fabricator.classes.success_get_resources_by_class(
            class_id="Paper"
        ),
    )
    def test_get_resources_by_class(self, *args):
        res = self.orkg.classes.get_resource_by_class(class_id="Paper")
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.classes.get_resource_by_class_unpaginated,
        return_value=Fabricator.classes.success_get_resources_by_class_unpaginated(
            class_id="Paper"
        ),
    )
    def test_get_resources_by_class_unpaginated(self, *args):
        term = "Paper"
        res = self.orkg.classes.get_resource_by_class_unpaginated(class_id=term)
        self.assertTrue(res.all_succeeded)

    @mock_test(
        orkg.classes.get_resource_by_class,
        return_value=Fabricator.classes.success_get_resources_by_class(
            class_id="Paper", size=30
        ),
    )
    def test_get_resources_by_class_with_items(self, *args):
        res = self.orkg.classes.get_resource_by_class(class_id="Paper", size=30)
        self.assertTrue(res.succeeded)
        self.assertEqual(len(res.content), 30)

    @mock_test(
        orkg.classes.get_resource_by_class,
        return_value=Fabricator.classes.success_get_resources_by_class_and_term(
            class_id="Problem", q="Machine"
        ),
    )
    def test_get_resources_by_class_with_query(self, *args):
        clazz = "Problem"
        term = "Machine"
        res = self.orkg.classes.get_resource_by_class(class_id=clazz, q=term)
        self.assertTrue(res.succeeded)
        self.assertTrue(
            all(
                term.lower().strip() in resource["label"].lower().strip()
                for resource in res.content
            )
        )

    @mock_test(
        orkg.classes.add, return_value=Fabricator.classes.success_add(label="Class Z")
    )
    def test_add(self, *args):
        label = "Class Z"
        res = self.orkg.classes.add(label=label)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)

    @mock_test(
        orkg.classes.add,
        return_value=Fabricator.classes.success_add(label="Cl@$$ L4B3L"),
    )
    @mock_test(
        orkg.classes.find_or_add,
        return_value=Fabricator.classes.success_find_or_add(label="Cl@$$ L4B3L"),
    )
    def test_find_or_add(self, *args):
        label = "Cl@$$ L4B3L"
        old = self.orkg.classes.add(label=label)
        self.assertTrue(old.succeeded, "Couldn't create the first  class")
        self.assertEqual(
            old.content["label"],
            label,
            "The label of the first class doesn't match the correct label",
        )
        new = self.orkg.classes.find_or_add(label=label)
        self.assertTrue(new.succeeded, "Couldn't find or create the second class")
        self.assertEqual(
            new.content["id"],
            old.content["id"],
            "The old and the new IDs are not the same",
        )

    @mock_test(
        orkg.classes.add,
        return_value=Fabricator.classes.success_add(id="Updatable", label="Class A"),
    )
    @mock_test(
        orkg.classes.update,
        return_value=Fabricator.classes.success_update(id="Updatable", label="Class B"),
    )
    @mock_test(
        orkg.classes.by_id,
        return_value=Fabricator.classes.success_by_id_from_cache(_id="Updatable"),
    )
    def test_update(self, *args):
        res = self.orkg.classes.add(label="Class A")
        self.assertTrue(res.succeeded)
        res = self.orkg.classes.update(id=res.content["id"], label="Class B")
        self.assertTrue(res.succeeded)
        res = self.orkg.classes.by_id(res.content["id"])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], "Class B")
