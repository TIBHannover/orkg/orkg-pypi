from unittest import TestCase

from orkg import ORKG, ComparisonType, Hosts, ThingType
from tests import Fabricator, mock_test


class TestComparisons(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(ORKG, "simcomp_available", return_value=Fabricator.boolean_true())
    @mock_test(
        orkg.comparisons.publish,
        return_value=Fabricator.resources.success_add(label="test comparison"),
    )
    @mock_test(orkg.json.get_json, return_value=Fabricator.success())
    def test_publish_live_comparison(self, *args):
        res = self.orkg.comparisons.publish(
            contribution_ids=["R339476", "R339468"],
            title="test comparison here",
            description="another test comparison",
        )
        self.assertTrue(res.succeeded, "Publishing comparison failed")
        self.assertTrue(
            res.content["label"] == "test comparison", "Comparison label is not correct"
        )
        simcomp_response = self.orkg.json.get_json(
            thing_key=res.content["id"], thing_type=ThingType.COMPARISON
        )
        self.assertTrue(
            simcomp_response.succeeded,
            f"Getting thing {res.content['id']} from SimComp failed",
        )

    @mock_test(ORKG, "simcomp_available", return_value=Fabricator.boolean_true())
    @mock_test(
        orkg.comparisons.publish,
        return_value=Fabricator.resources.success_add(label="test comparison"),
    )
    @mock_test(orkg.json.get_json, return_value=Fabricator.success())
    @mock_test(
        orkg.contributions.compare,
        return_value=Fabricator.contributions.success_comparison_response(
            **{"predicates": ["P32"]}
        ),
    )
    def test_publish_pre_computed_comparison(self, *args):
        data = self.orkg.contributions.compare(
            contributions=["R288053", "R288056"],
            comparison_type=ComparisonType.MERGE,
        ).content["payload"]["comparison"]
        self.assertTrue(isinstance(data, dict), "Comparison data is not a dictionary")
        res = self.orkg.comparisons.publish(
            title="test comparison",
            description="another test comparison",
            data=data,
        )
        self.assertTrue(res.succeeded, "Publishing comparison failed")
        self.assertTrue(
            res.content["label"] == "test comparison", "Comparison label is not correct"
        )
        simcomp_response = self.orkg.json.get_json(
            thing_key=res.content["id"], thing_type=ThingType.COMPARISON
        )
        self.assertTrue(
            simcomp_response.succeeded,
            f"Getting thing {res.content['id']} from SimComp failed",
        )

    @mock_test(
        orkg.comparisons.in_research_field,
        return_value=Fabricator.classes.success_get_resources_by_class(
            size=5, class_id="Comparison"
        ),
    )
    def test_in_research_fields(self, *args) -> None:
        size = 5
        res = self.orkg.comparisons.in_research_field("R132", size=size)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue(
            "Comparison" in res.content[0]["classes"], "Response is not a comparison"
        )
        self.assertEqual(
            len(res.content), size, "Content length does not match requested size"
        )

    @mock_test(
        orkg.comparisons.in_research_field,
        return_value=Fabricator.classes.success_get_resources_by_class(
            class_id="Comparison"
        ),
    )
    def test_in_research_fields_with_subfields(self, *args) -> None:
        res = self.orkg.comparisons.in_research_field(
            "R132", include_subfields=True, size=3
        )
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue(
            "Comparison" in res.content[0]["classes"], "Response is not a comparison"
        )

    @mock_test(
        orkg.comparisons.in_research_field,
        return_value=Fabricator.resources.success_get(size=0),
    )
    def test_in_research_fields_no_comparisons(self, *args) -> None:
        # id = 'R333090'  # not research field
        id = "R397"  # French linguistics, no papers
        res = self.orkg.comparisons.in_research_field(id, include_subfields=True)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(len(res.content), 0, "Unexpected content returned!")
