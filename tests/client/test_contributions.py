from unittest import TestCase

import pandas as pd

from orkg import ORKG, ExportFormat, Hosts
from tests import Fabricator, mock_test


class TestContributions(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(ORKG, "simcomp_available", return_value=Fabricator.boolean_true())
    def test_check_simcomp(self, *args):
        self.assertTrue(self.orkg.simcomp_available)

    @mock_test(orkg.contributions.similar, return_value=Fabricator.success())
    def test_get_similar(self, *args):
        res = self.orkg.contributions.similar("R288056")
        self.assertTrue(res.succeeded, "Similar contributions request failed")

    @mock_test(
        orkg.contributions.compare,
        return_value=Fabricator.contributions.success_comparison_response(),
    )
    def test_get_comparison(self, *args):
        res = self.orkg.contributions.compare(["R288056", "R288053"])
        self.assertTrue(res.succeeded)
        self.assertTrue("payload" in res.content, "No payload in response")
        self.assertTrue(
            "comparison" in res.content["payload"], "No comparison in payload"
        )
        self.assertTrue(
            res.content["payload"]["comparison"] is not None, "Comparison is None"
        )

    @mock_test(
        orkg.contributions.compare,
        return_value=Fabricator.contributions.success_comparison_csv_response(),
    )
    def test_get_comparison_csv_export(self, *args):
        res = self.orkg.contributions.compare(
            ["R288056", "R288053"], export_format=ExportFormat.CSV
        )
        self.assertTrue(res.succeeded, "Comparison CSV export request failed")
        self.assertTrue(
            isinstance(res.content, str), "Comparison CSV export is not a string"
        )
        self.assertTrue(
            self._is_valid_csv(res.content), "Comparison CSV export is not valid CSV"
        )

    @staticmethod
    def _is_valid_csv(csv_content: str) -> bool:
        import csv
        from io import StringIO

        try:
            # Create a CSV reader object using StringIO to simulate a file
            csv_reader = csv.reader(StringIO(csv_content))
            # Attempt to read the first row
            next(csv_reader)
            # If successful, it's a valid CSV
            return True
        except csv.Error:
            # If there's an error, it's not a valid CSV
            return False

    @mock_test(
        orkg.contributions.compare_dataframe, return_value=Fabricator.dataframe()
    )
    def test_get_comparison_df_without_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            contributions=["R288056", "R288053"]
        )
        self.assertIsInstance(
            res, pd.DataFrame, "Comparison dataframe is not a pandas dataframe"
        )

    @mock_test(
        orkg.contributions.compare_dataframe, return_value=Fabricator.dataframe()
    )
    def test_get_comparison_df_on_comparison_without_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(comparison_id="R334250")
        self.assertIsInstance(
            res, pd.DataFrame, "Comparison dataframe is not a pandas dataframe"
        )

    @mock_test(
        orkg.contributions.compare_dataframe,
        return_value=Fabricator.contributions.dataframe_with_metadata(),
    )
    def test_get_comparison_df_with_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            contributions=["R288056", "R288053"], include_meta=True
        )
        self.assertIsInstance(res, tuple, "Comparison dataframe is not a tuple")
        self.assertTrue(
            (res[0].columns == res[1].columns).all(), "Columns are not equal"
        )
        self.assertTrue(
            "paper id" in res[1].index and "contribution id" in res[1].index,
            "Paper id or contribution id are not in the index",
        )
        self.assertTrue(
            res[1].loc["contribution id"].is_unique, "Contribution id is not unique"
        )

    @mock_test(
        orkg.contributions.compare_dataframe,
        return_value=Fabricator.contributions.dataframe_with_metadata(),
    )
    def test_get_comparison_df_on_comparison_with_metadata(self, *args):
        res = self.orkg.contributions.compare_dataframe(
            comparison_id="R334250", include_meta=True, like_ui=False
        )
        self.assertIsInstance(res, tuple, "Comparison dataframe is not a tuple")
        self.assertTrue(
            (res[0].columns == res[1].columns).all(), "Columns are not equal"
        )
        self.assertTrue(
            "paper id" in res[1].index and "contribution id" in res[1].index,
            "Paper id or contribution id are not in the index",
        )
        self.assertTrue(
            res[1].loc["contribution id"].is_unique, "Contribution id is not unique"
        )
