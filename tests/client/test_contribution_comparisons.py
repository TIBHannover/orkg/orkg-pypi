from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestContributionComparisons(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.contribution_comparisons.by_ids, return_value=Fabricator.success())
    def test_by_ids(self, *args):
        res = self.orkg.contribution_comparisons.by_ids(ids=["R166186", "R166180"])
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.contribution_comparisons.by_ids_unpaginated,
        return_value=Fabricator.success_all(),
    )
    def test_by_ids_unpaginated(self, *args):
        size = 10
        res = self.orkg.contribution_comparisons.by_ids_unpaginated(
            ids=["R166186", "R166180"] * size, size=size, end_page=5
        )
        self.assertTrue(res.all_succeeded)
