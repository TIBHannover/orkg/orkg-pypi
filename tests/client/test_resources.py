from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestResources(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.resources.by_id, return_value=Fabricator.resources.success_by_id("R1")
    )
    def test_by_id(self, *args):
        res = self.orkg.resources.by_id("R1")
        self.assertTrue(res.succeeded)

    @mock_test(orkg.resources.get, return_value=Fabricator.resources.success_get(10))
    def test_get(self, *args):
        size = 10
        res = self.orkg.resources.get(size=size)
        self.assertTrue(res.succeeded)
        self.assertEqual(len(res.content), size)

    @mock_test(
        orkg.resources.get_unpaginated, return_value=Fabricator.resources.success_all()
    )
    def test_get_unpaginated(self, *args):
        size = 500
        res = self.orkg.resources.get_unpaginated(size=size, end_page=5)
        self.assertTrue(res.all_succeeded)

    @mock_test(
        orkg.resources.get,
        return_value=Fabricator.resources.success_get(10),
    )
    def test_get_resources_by_class(self, *args):
        res = self.orkg.resources.get(include="Paper")
        self.assertTrue(res.succeeded)

    @mock_test(orkg.resources.exists, return_value=Fabricator.boolean_false())
    @mock_test(
        orkg.resources.add, return_value=Fabricator.resources.success_add(id="DeleteMe")
    )
    @mock_test(
        orkg.resources.delete,
        return_value=Fabricator.resources.success_delete("DeleteMe"),
    )
    def test_delete(self, *args):
        to_delete = self.orkg.resources.add(label="I should be deleted")
        self.assertTrue(to_delete.succeeded)
        res = self.orkg.resources.delete(id=to_delete.content["id"])
        self.assertTrue(res.succeeded)
        self.assertFalse(self.orkg.resources.exists(to_delete.content["id"]))

    @mock_test(
        orkg.resources.add, return_value=Fabricator.resources.success_add(label="test")
    )
    def test_add(self, *args):
        label = "test"
        res = self.orkg.resources.add(label=label)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(label="Rand0M L4b3l"),
    )
    @mock_test(
        orkg.resources.find_or_add,
        return_value=Fabricator.resources.success_find_or_add(label="Rand0M L4b3l"),
    )
    def test_find_or_add_with_label(self, *args):
        label = "Rand0M L4b3l"
        old = self.orkg.resources.add(label=label)
        self.assertTrue(old.succeeded, "Couldn't create the first resource")
        self.assertEqual(
            old.content["label"],
            label,
            "The label of the first resource doesn't match the correct label",
        )
        new = self.orkg.resources.find_or_add(label=label)
        self.assertTrue(new.succeeded, "Couldn't find or create the second resource")
        self.assertEqual(
            new.content["id"],
            old.content["id"],
            "The old and the new IDs are not the same",
        )

    @mock_test(
        orkg.classes.get_all,
        return_value=Fabricator.classes.success_get_one_as_list(id="ClaZZ"),
    )
    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(
            label="Rand0M L4b3l", classes=["ClaZZ"]
        ),
    )
    @mock_test(
        orkg.resources.find_or_add,
        return_value=Fabricator.resources.success_find_or_add(
            label="Rand0M L4b3l", classes=["ClaZZ"]
        ),
    )
    def test_find_or_add_with_label_and_class(self, *args):
        label = "Rand0M L4b3l"

        lst_of_classes = self.orkg.classes.get_all()
        class_id = lst_of_classes.content[0]["id"]
        old = self.orkg.resources.add(label=label, classes=[class_id])
        self.assertTrue(old.succeeded, "Couldn't create the first resource")
        self.assertEqual(
            old.content["label"],
            label,
            "The label of the first resource doesn't match the correct label",
        )
        self.assertEqual(
            old.content["classes"],
            [class_id],
            "The class of the first resource doesn't match the correct class",
        )
        new = self.orkg.resources.find_or_add(label=label, classes=[class_id])
        self.assertTrue(new.succeeded, "Couldn't create second resource")
        self.assertEqual(
            new.content["id"],
            old.content["id"],
            "The old and the new IDs are not the same",
        )

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(label="test", classes=["Coco"]),
    )
    def test_add_with_class(self, *args):
        label = "test"
        clazz = "Coco"
        res = self.orkg.resources.add(label=label, classes=[clazz])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)
        self.assertIn(clazz, res.content["classes"])

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(id="Updatable", label="First"),
    )
    @mock_test(
        orkg.resources.update,
        return_value=Fabricator.resources.success_add(id="Updatable", label="Second"),
    )
    @mock_test(
        orkg.resources.by_id,
        return_value=Fabricator.resources.success_by_id_from_cache(_id="Updatable"),
    )
    def test_update(self, *args):
        res = self.orkg.resources.add(label="First")
        self.assertTrue(res.succeeded)
        res = self.orkg.resources.update(id=res.content["id"], label="Second")
        self.assertTrue(res.succeeded)
        res = self.orkg.resources.by_id(res.content["id"])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], "Second")

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(
            id="Updatable", label="test", extraction_method="AUTOMATIC"
        ),
    )
    def test_add_resource_with_extraction_method(self, *args):
        label = "test"
        extraction_method = "AUTOMATIC"
        res = self.orkg.resources.add(label=label, extraction_method=extraction_method)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)
        self.assertEqual(res.content["extraction_method"], extraction_method)

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(
            id="Updatable", label="First", extraction_method="UNKNOWN"
        ),
    )
    @mock_test(
        orkg.resources.update,
        return_value=Fabricator.resources.success_add(
            id="Updatable", extraction_method="AUTOMATIC"
        ),
    )
    @mock_test(
        orkg.resources.by_id,
        return_value=Fabricator.resources.success_by_id_from_cache(_id="Updatable"),
    )
    def test_update_resource_with_extraction_method(self, *args):
        res = self.orkg.resources.add(label="First")
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["extraction_method"], "UNKNOWN")
        res = self.orkg.resources.update(
            id=res.content["id"], extraction_method="AUTOMATIC"
        )
        self.assertTrue(res.succeeded)
        res = self.orkg.resources.by_id(res.content["id"])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["extraction_method"], "AUTOMATIC")
