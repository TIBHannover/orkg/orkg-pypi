from unittest import TestCase

from orkg import ORKG, Hosts, ThingType
from tests import Fabricator, mock_test


class TestJSONSimcomp(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.json.get_json, return_value=Fabricator.success())
    def test_get(self, *args):
        res = self.orkg.json.get_json(
            thing_key="R307226", thing_type=ThingType.COMPARISON
        )
        self.assertTrue(res.succeeded, "JSON get request failed")

    @mock_test(orkg.json.save_json, return_value=Fabricator.success())
    @mock_test(
        orkg.json.get_json,
        return_value=Fabricator.jsons.success_get(
            **{"payload": {"thing": {"data": {"test": "test"}}}}
        ),
    )
    def test_add(self, *args):
        res = self.orkg.json.save_json(
            thing_key="RRR2222", thing_type=ThingType.ANY, data={"test": "test"}
        )
        self.assertTrue(res.succeeded, "JSON save request failed")
        res = self.orkg.json.get_json(thing_key="RRR2222", thing_type=ThingType.ANY)
        self.assertTrue(res.succeeded, "JSON get request failed")
        self.assertEqual(
            res.content["payload"]["thing"]["data"],
            {"test": "test"},
            "JSON data is not correct",
        )
