from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestResearchProblems(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.problems.in_research_field,
        return_value=Fabricator.problems.success_get_problem_paper_pair_in_field(),
    )
    def test_problems_in_fields(self, *args) -> None:
        res = self.orkg.problems.in_research_field("R132")
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue(
            "papers" in res.content[0], "Content does not contain the papers field"
        )
        self.assertTrue(
            "problem" in res.content[0], "Content does not contain the problem field"
        )

    @mock_test(
        orkg.problems.in_research_field,
        return_value=Fabricator.classes.success_get_resources_by_class(
            class_id="Problem"
        ),
    )
    def test_problems_in_fields_with_subfields(self, *args) -> None:
        res = self.orkg.problems.in_research_field("R132", include_subfields=True)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue(
            "Problem" in res.content[0]["classes"], "Response is not a problem"
        )

    @mock_test(
        orkg.problems.in_research_field,
        return_value=Fabricator.problems.success_get_problem_paper_pairs_in_field(
            size=2
        ),
    )
    def test_problems_in_fields_size_two(self, *args) -> None:
        res = self.orkg.problems.in_research_field("R132", size=2)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) == 2, "Requested content size is not 2")
        self.assertTrue(
            "papers" in res.content[0], "Content does not contain the papers field"
        )
        self.assertTrue(
            "problem" in res.content[0], "Content does not contain the problem field"
        )
