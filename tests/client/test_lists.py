from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestLists(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.lists.by_id, return_value=Fabricator.lists.success_by_id("R315000"))
    def test_by_id(self, *args):
        res = self.orkg.lists.by_id("R315000")
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")

    @mock_test(orkg.lists.by_id, side_effect=ValueError)
    def test_by_id_nonlist(self, *args):
        with self.assertRaises(ValueError):
            self.orkg.lists.by_id("R123")

    @mock_test(
        orkg.lists.get_elements, return_value=Fabricator.literals.success_get_all(5)
    )
    def test_get_elements(self, *args):
        res = self.orkg.lists.get_elements("R338045")
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")

    @mock_test(
        orkg.lists.get_elements, return_value=Fabricator.literals.success_get_all(3)
    )
    def test_get_elements_with_queryparams(self, *args):
        res = self.orkg.lists.get_elements("R338045", size=3)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) == 3, "Requested content size is not 3")

    @mock_test(orkg.lists.get_elements, side_effect=ValueError)
    def test_get_elements_nonlist(self, *args):
        with self.assertRaises(ValueError):
            self.orkg.lists.get_elements("R123")

    @mock_test(
        orkg.lists.add,
        return_value=Fabricator.lists.success_add(
            label="my list", elements=["R1", "R2"]
        ),
    )
    def test_add(self, *args):
        label = "my list"
        elements = ["R1", "R2"]
        res = self.orkg.lists.add(label=label, elements=elements)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(res.content["label"], label)
        self.assertEqual(res.content["elements"], elements)

    @mock_test(
        orkg.lists.add,
        return_value=Fabricator.lists.success_add(
            id="Updatable", label="first", elements=["R1", "R2"]
        ),
    )
    @mock_test(
        orkg.lists.update,
        return_value=Fabricator.lists.success_update(
            id="Updatable", label="second", elements=["R3", "R4"]
        ),
    )
    @mock_test(
        orkg.lists.by_id,
        return_value=Fabricator.lists.success_by_id_from_cache(_id="Updatable"),
    )
    def test_update(self, *args):
        res = self.orkg.lists.add(label="first", elements=["R1", "R2"])
        list_id = res.content["id"]
        label = "second"
        elements = ["R3", "R4"]
        res = self.orkg.lists.update(list_id=list_id, label=label, elements=elements)
        self.assertTrue(res.succeeded, "Request failed")
        res = self.orkg.lists.by_id(list_id)
        self.assertEqual(res.content["label"], label)
        self.assertEqual(res.content["elements"], elements)

    @mock_test(orkg.lists.update, side_effect=ValueError)
    def test_update_nonlist(self, *args):
        with self.assertRaises(ValueError):
            self.orkg.lists.update(list_id="R123", label="new")
