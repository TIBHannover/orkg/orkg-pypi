import warnings
from unittest import TestCase

import pandas as pd

from orkg import ORKG, Hosts
from orkg.out import OrkgResponse
from tests import Fabricator, mock_test


class TestOut(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.pagination_activated,
        return_value=Fabricator.boolean_true(),
    )
    @mock_test(
        orkg.resources.get,
        return_value=Fabricator.success_pageable(),
    )
    def test_new_pagination_wrapper_pagination_active(self, *args):
        res = self.orkg.resources.get()
        self.assertTrue(
            self.orkg.pagination_activated(), "pagination isn't active on this backend"
        )
        self.assertIsNotNone(res.page_info, "pageable info is None")
        self.assertTrue(
            "content" not in res.content,
            "content extraction was not performed correctly",
        )

    # TODO: figure out why adding autospec=True as a parameter with fail_pageable causes the test to fail
    # https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/59#note_1633496866
    @mock_test(
        orkg.pagination_activated,
        return_value=Fabricator.boolean_false(),
    )
    @mock_test(
        orkg.resources.get, return_value=Fabricator.fail_pageable(DeprecationWarning)
    )
    def test_new_pagination_wrapper_pagination_not_active(self, *args):
        res = self.orkg.resources.get()
        self.assertFalse(self.orkg.pagination_activated())
        self.assertIsNone(
            res.page_info,
            "pageable info is None because back-end is not up to date",
        )
        self.assertWarns(DeprecationWarning)

    @mock_test(
        orkg.pagination_activated,
        return_value=Fabricator.boolean_true(),
    )
    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(label="test", pageable=False),
    )
    def test_new_pagination_wrapper_on_none_get_call_pagination_active(self, *args):
        res = self.orkg.resources.add(label="test")
        self.assertTrue(res.succeeded)
        self.assertTrue(self.orkg.pagination_activated())
        self.assertIsNone(res.page_info, "pageable is not available in POST calls")
        self.assertTrue(
            "content" not in res.content, "content extraction performed correctly"
        )

    @mock_test(
        orkg.pagination_activated,
        return_value=Fabricator.boolean_false(),
        side_effect=lambda: warnings.warn("This is a warning", DeprecationWarning),
    )
    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(label="test", pageable=False),
    )
    def test_new_pagination_wrapper_on_none_get_call_pagination_inactive(self, *args):
        res = self.orkg.resources.add(label="test")
        self.assertTrue(res.succeeded)
        self.assertFalse(self.orkg.pagination_activated())
        self.assertIsNone(
            res.page_info,
            "pageable is not available in POST calls even if the backend is up-to-date",
        )
        self.assertWarns(DeprecationWarning)

    @mock_test(
        orkg.resources.by_id,
        return_value=orkg.dummy.create_200_response({}),
    )
    @mock_test(
        OrkgResponse,
        "as_dataframe",
        return_value=Fabricator.dataframe(),
    )
    def test_casting_output_as_df(self, *args):
        x = self.orkg.resources.by_id(id="R415463").as_dataframe()
        self.assertIsInstance(x, pd.DataFrame)

    @mock_test(
        orkg.resources.by_id,
        side_effect=ValueError,
    )
    def test_casting_output_as_unsupported_throws_an_error(self, *args):
        with self.assertRaises(ValueError):
            self.orkg.resources.by_id(id="R415463").as_type(bool)
