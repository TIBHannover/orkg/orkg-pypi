from unittest import TestCase, skip

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestPapers(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.papers.by_id, return_value=Fabricator.papers.success_by_id("R1"))
    def test_by_id(self, *args):
        res = self.orkg.papers.by_id("R8186")
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.get, return_value=Fabricator.papers.success_get(5))
    def test_get(self, *args):
        size = 5
        res = self.orkg.papers.get(
            title="covid", visibility="UNLISTED", size=size, research_field="R33"
        )
        self.assertTrue(res.succeeded)
        self.assertEqual(len(res.content), size)

    @mock_test(
        orkg.papers.get_unpaginated, return_value=Fabricator.papers.success_all()
    )
    def test_get_unpaginated(self, *args):
        start_page = 2
        end_page = 5
        res = self.orkg.papers.get_unpaginated(
            title="covid", start_page=start_page, end_page=end_page
        )
        self.assertTrue(res.all_succeeded)

    @mock_test(
        orkg.papers.get_contributors,
        return_value=Fabricator.papers.success_get_contributors(),
    )
    def test_get_contributors(self, *args):
        paper_id = "R8186"
        res = self.orkg.papers.get_contributors(paper_id=paper_id)
        self.assertTrue(res.succeeded)
        self.assertGreaterEqual(len(res.content), 1)

    @mock_test(orkg.papers.update, return_value=Fabricator.success())
    def test_update(self, *args):
        paper_id = "R364915"
        title = "Example Paper"
        research_fields = ["R11"]
        res = self.orkg.papers.update(
            paper_id=paper_id, title=title, research_fields=research_fields
        )
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.publish, return_value=Fabricator.success())
    def test_publish(self, *args):
        paper_id = "R364907"
        subject = "test"
        description = "test"
        authors = [
            {
                "homepage": None,
                "id": None,
                "identifiers": None,
                "name": "John Smith",
            }
        ]
        res = self.orkg.papers.publish(
            paper_id=paper_id, subject=subject, description=description, authors=authors
        )
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.add, return_value=Fabricator.papers.success_add())
    def test_add(self, *args):
        paper = Fabricator.samples.paper_sample()
        res = self.orkg.papers.add(params=paper)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.add, side_effect=TypeError)
    def test_add_with_missing_param(self, *args):
        paper = Fabricator.samples.paper_sample()
        del paper["contents"]
        with self.assertRaises(TypeError):
            self.orkg.papers.add(params=paper)

    @mock_test(
        orkg.papers.add_v1,
        return_value=Fabricator.resources.success_add(classes=["Paper"]),
    )
    def test_add_legacy(self, *args):
        paper = Fabricator.samples.legacy_paper_sample()
        res = self.orkg.papers.add(params=paper)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.add, side_effect=TypeError)
    def test_add_legacy_with_missing_param(self, *args):
        paper = Fabricator.samples.legacy_paper_sample()
        del paper["paper"]["title"]
        with self.assertRaises(TypeError):
            self.orkg.papers.add(params=paper)

    @mock_test(
        orkg.papers.add_v1,
        return_value=Fabricator.resources.success_add(classes=["Paper"]),
    )
    def test_add_v1(self, *args):
        paper = Fabricator.samples.legacy_paper_sample()
        res = self.orkg.papers.add_v1(params=paper)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.add_v2, return_value=Fabricator.papers.success_add())
    def test_add_v2(self, *args):
        paper = Fabricator.samples.paper_sample()
        res = self.orkg.papers.add_v2(**paper)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.by_doi, return_value=Fabricator.success())
    def test_get_paper_by_doi(self, *args):
        # Without "https://doi.org/"
        doi = Fabricator.samples.doi_sample()
        res = self.orkg.papers.by_doi(doi)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.by_doi, return_value=Fabricator.success())
    def test_get_paper_by_full_url_doi(self, *args):
        # With "https://doi.org/"
        doi_with_url = Fabricator.samples.doi_sample(make_url=True)
        res = self.orkg.papers.by_doi(doi_with_url)
        self.assertTrue(res.succeeded)

    @mock_test(orkg.papers.by_title, return_value=Fabricator.success())
    def test_get_paper_by_title(self, *args):
        title = Fabricator.samples.title_sample()
        res = self.orkg.papers.by_title(title)
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.papers.in_research_field,
        return_value=Fabricator.classes.success_get_resources_by_class(
            size=5, class_id="Paper"
        ),
    )
    def test_in_research_fields(self, *args) -> None:
        size = 5
        res = self.orkg.papers.in_research_field("R132", size=size)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue("Paper" in res.content[0]["classes"], "Response is not a paper")
        self.assertEqual(
            len(res.content), size, "Content length does not match requested size"
        )

    @mock_test(
        orkg.papers.in_research_field,
        return_value=Fabricator.classes.success_get_resources_by_class(
            class_id="Paper"
        ),
    )
    def test_in_research_fields_with_subfields(self, *args) -> None:
        res = self.orkg.papers.in_research_field("R132", include_subfields=True)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertTrue(len(res.content) > 0, "Content is empty")
        self.assertTrue("Paper" in res.content[0]["classes"], "Response is not a paper")

    @mock_test(
        orkg.papers.in_research_field,
        return_value=Fabricator.resources.success_get(size=0),
    )
    def test_in_research_fields_no_papers(self, *args) -> None:
        # id = 'R333090'  # not research field
        id = "R397"  # French linguistics, no papers
        res = self.orkg.papers.in_research_field(id, include_subfields=True)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(len(res.content), 0, "Unexpected content returned!")
