from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestStatements(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.statements.by_id,
        return_value=Fabricator.statements.success_by_id("S1"),
    )
    def test_by_id(self, *args):
        res = self.orkg.statements.by_id("S1")
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get,
        return_value=Fabricator.statements.success_get(10),
    )
    def test_get(self, *args):
        size = 10
        res = self.orkg.statements.get(size=size)
        self.assertTrue(res.succeeded)
        self.assertEqual(len(res.content), size)

    @mock_test(
        orkg.statements.get_unpaginated,
        return_value=Fabricator.statements.success_get_all(10),
    )
    def test_get_unpaginated(self, *args):
        size = 500
        res = self.orkg.statements.get_unpaginated(size=size, end_page=5)
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.statements.get_by_subject,
        return_value=Fabricator.statements.success_get_by_subject("R180000"),
    )
    def test_get_by_subject(self, *args):
        size = 10
        subject_id = "R180000"
        res = self.orkg.statements.get_by_subject(subject_id=subject_id, size=size)
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get_by_subject_unpaginated,
        return_value=Fabricator.statements.success_get_all(),
    )
    def test_get_by_subject_unpaginated(self, *args):
        size = 10
        subject_id = "R180000"
        res = self.orkg.statements.get_by_subject_unpaginated(
            subject_id=subject_id, size=size, end_page=5
        )
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.statements.get_by_predicate,
        return_value=Fabricator.statements.success_get_by_predicate("P22"),
    )
    def test_get_by_predicate(self, *args):
        size = 10
        predicate_id = "P22"
        res = self.orkg.statements.get_by_predicate(
            predicate_id=predicate_id, size=size
        )
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get_by_predicate_unpaginated,
        return_value=Fabricator.statements.success_get_all(),
    )
    def test_get_by_predicate_unpaginated(self, *args):
        size = 10
        predicate_id = "P26"
        res = self.orkg.statements.get_by_predicate_unpaginated(
            predicate_id=predicate_id, size=size, end_page=5
        )
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.statements.get_by_object,
        return_value=Fabricator.statements.success_get_by_object("R57"),
    )
    def test_get_by_object(self, *args):
        size = 10
        object_id = "R57"
        res = self.orkg.statements.get_by_object(object_id=object_id, size=size)
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get_by_object_unpaginated,
        return_value=Fabricator.statements.success_get_all(),
    )
    def test_get_by_object_unpaginated(self, *args):
        size = 10
        object_id = "R57"
        res = self.orkg.statements.get_by_object_unpaginated(
            object_id=object_id, size=size, end_page=5
        )
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.statements.get_by_object_and_predicate,
        return_value=Fabricator.statements.success_get_by_object_and_predicate(
            "R57", "P30"
        ),
    )
    def test_get_by_object_and_predicate(self, *args):
        size = 10
        object_id = "R57"
        predicate_id = "P30"
        res = self.orkg.statements.get_by_object_and_predicate(
            object_id=object_id, predicate_id=predicate_id, size=size
        )
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get_by_object_and_predicate_unpaginated,
        return_value=Fabricator.statements.success_get_all(),
    )
    def test_get_by_object_and_predicate_unpaginated(self, *args):
        size = 10
        object_id = "R57"
        predicate_id = "P30"
        res = self.orkg.statements.get_by_object_and_predicate_unpaginated(
            object_id=object_id, predicate_id=predicate_id, size=size
        )
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.statements.get_by_subject_and_predicate,
        return_value=Fabricator.statements.success_get_by_subject_and_predicate(
            "R180000", "compareContribution"
        ),
    )
    def test_get_by_subject_and_predicate(self, *args):
        size = 10
        subject_id = "R180000"
        predicate_id = "compareContribution"
        res = self.orkg.statements.get_by_subject_and_predicate(
            subject_id=subject_id, predicate_id=predicate_id, size=size
        )
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.get_by_subject_and_predicate_unpaginated,
        return_value=Fabricator.statements.success_get_all(),
    )
    def test_get_by_subject_and_predicate_unpaginated(self, *args):
        size = 10
        subject_id = "R0"
        predicate_id = "P0"
        res = self.orkg.statements.get_by_subject_and_predicate_unpaginated(
            subject_id=subject_id, predicate_id=predicate_id, size=size
        )
        self.assertTrue(res.all_succeeded)
        self.assertTrue(len(res.responses) > 1)

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(
            id="Subject", label="ORKG Subject"
        ),
    )
    @mock_test(
        orkg.predicates.add,
        return_value=Fabricator.predicates.success_add(
            id="Predicate", label="ORKG Predicate"
        ),
    )
    @mock_test(
        orkg.literals.add,
        return_value=Fabricator.literals.success_add(id="Object", label="ORKG Object"),
    )
    @mock_test(
        orkg.statements.add,
        return_value=Fabricator.statements.success_add_ids(
            subject_id="Subject", predicate_id="Predicate", object_id="Object"
        ),
    )
    def test_add(self, *args):
        subject = self.orkg.resources.add(label="ORKG Subject").content["id"]
        predicate = self.orkg.predicates.add(label="ORKG Predicate").content["id"]
        object = self.orkg.literals.add(label="ORKG Object").content["id"]
        res = self.orkg.statements.add(
            subject_id=subject, predicate_id=predicate, object_id=object
        )
        self.assertTrue(res.succeeded)

    @mock_test(
        orkg.statements.update,
        return_value=Fabricator.statements.success_update_predicate(
            id="Updatable", predicate_id="P1"
        ),
    )
    @mock_test(
        orkg.statements.by_id,
        return_value=Fabricator.statements.success_by_id_from_cache("Updatable"),
    )
    def test_update(self, *args):
        st_id = "Updatable"
        new_p_id = "P1"
        res = self.orkg.statements.update(id=st_id, predicate_id=new_p_id)
        self.assertTrue(res.succeeded)
        res = self.orkg.statements.by_id(st_id)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["predicate"]["id"], new_p_id)

    @mock_test(orkg.statements.exists, return_value=Fabricator.boolean_true())
    def test_exists(self, *args):
        found = self.orkg.statements.exists(id="S1")
        self.assertTrue(found)

    @mock_test(orkg.statements.exists, return_value=Fabricator.boolean_false())
    def test_not_exists(self, *args):
        found = self.orkg.statements.exists(id="SS1")
        self.assertFalse(found)

    @mock_test(
        orkg.resources.add,
        return_value=Fabricator.resources.success_add(
            id="Subject", label="ORKG Subject"
        ),
    )
    @mock_test(
        orkg.predicates.add,
        return_value=Fabricator.predicates.success_add(
            id="Predicate", label="ORKG Predicate"
        ),
    )
    @mock_test(
        orkg.literals.add,
        return_value=Fabricator.literals.success_add(id="Object", label="ORKG Object"),
    )
    @mock_test(
        orkg.statements.add,
        return_value=Fabricator.statements.success_add_ids(
            subject_id="Subject", predicate_id="Predicate", object_id="Object"
        ),
    )
    @mock_test(
        orkg.statements.delete,
        return_value=Fabricator.success(),
    )
    @mock_test(
        orkg.statements.exists,
        return_value=Fabricator.boolean_false(),
    )
    def test_delete(self, *args):
        subject = self.orkg.resources.add(label="ORKG Subject").content["id"]
        predicate = self.orkg.predicates.add(label="ORKG Predicate").content["id"]
        object = self.orkg.literals.add(label="ORKG Object").content["id"]
        res = self.orkg.statements.add(
            subject_id=subject, predicate_id=predicate, object_id=object
        )
        st_id = res.content["id"]
        self.assertTrue(res.succeeded)
        res = self.orkg.statements.delete(id=st_id)
        self.assertTrue(res.succeeded)
        found = self.orkg.statements.exists(id=st_id)
        self.assertFalse(found)
