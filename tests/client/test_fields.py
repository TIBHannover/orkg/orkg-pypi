from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestFields(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.fields.with_benchmarks, return_value=Fabricator.success())
    def test_success_with_benchmarks(self, *args):
        res = self.orkg.fields.with_benchmarks()
        self.assertTrue(res.succeeded, "Request failed")

    @mock_test(
        orkg.fields.with_benchmarks,
        return_value=Fabricator.resources.success_get(size=2),
    )
    def test_success_with_benchmarks_paginated(self, *args):
        res = self.orkg.fields.with_benchmarks(size=2)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(
            len(res.content), 2, "Content length does not match requested size"
        )

    @mock_test(orkg.fields.with_benchmarks, return_value=Fabricator.fail())
    def test_fail_with_benchmarks(self, *args):
        res = self.orkg.fields.with_benchmarks()
        self.assertFalse(res.succeeded, "Request succeeded??")

    @mock_test(
        orkg.fields.get_subfields, return_value=Fabricator.resources.success_get(size=3)
    )
    def test_get_subfields(self, *args):
        field_id = "R230"
        size = 3
        res = self.orkg.fields.get_subfields(field_id, size=size)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(
            len(res.content), size, "Content length does not match requested size"
        )

    @mock_test(orkg.fields.get_subfields, return_value=Fabricator.fail())
    def test_get_subfields_nonfield(self, *args):
        field_id = "R281058"  # not research field
        res = self.orkg.fields.get_subfields(field_id)
        self.assertFalse(res.succeeded, "Request succeeded??")

    @mock_test(
        orkg.fields.get_subfields, return_value=Fabricator.resources.success_get(size=0)
    )
    def test_get_subfields_no_subfields(self, *args):
        field_id = "R161726"  # research field with no subfields
        res = self.orkg.fields.get_subfields(field_id)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(len(res.content), 0, "Unexpected content returned!")

    @mock_test(
        orkg.fields.get_superfields,
        return_value=Fabricator.resources.success_get(size=2),
    )
    def test_get_superfields(self, *args):
        field_id = "R137546"
        size = 2
        res = self.orkg.fields.get_superfields(field_id, size=size)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(
            len(res.content), size, "Content length does not match requested size"
        )

    @mock_test(orkg.fields.get_superfields, return_value=Fabricator.fail())
    def test_get_superfields_nonfield(self, *args):
        field_id = "R281058"  # not research field
        res = self.orkg.fields.get_superfields(field_id)
        self.assertFalse(res.succeeded, "Request succeeded??")

    @mock_test(
        orkg.fields.get_superfields,
        return_value=Fabricator.resources.success_get(size=0),
    )
    def test_get_superfields_no_superfields(self, *args):
        field_id = "R11"  # research field with no superfield
        res = self.orkg.fields.get_superfields(field_id)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(len(res.content), 0, "Unexpected content returned!")

    @mock_test(
        orkg.fields.get_root, return_value=Fabricator.resources.success_get(size=1)
    )
    def test_get_root(self, *args):
        field_id = "R136088"
        res = self.orkg.fields.get_root(field_id)
        self.assertTrue(res.succeeded, "Request failed")

    @mock_test(orkg.fields.get_root, return_value=Fabricator.fail())
    def test_get_root_nonfield(self, *args):
        field_id = "R281058"  # not research field
        res = self.orkg.fields.get_root(field_id)
        self.assertFalse(res.succeeded, "Request succeeded??")

    @mock_test(
        orkg.fields.get_root, return_value=Fabricator.resources.success_get(size=0)
    )
    def test_get_root_on_root(self, *args):
        field_id = "R11"  # one root to rule them all
        res = self.orkg.fields.get_root(field_id)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(len(res.content), 0, "Unexpected content returned!")

    @mock_test(
        orkg.fields.get_all_roots, return_value=Fabricator.resources.success_get(size=2)
    )
    def test_get_all_roots(self, *args):
        size = 2
        res = self.orkg.fields.get_all_roots(size=size)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(
            len(res.content), size, "Content length does not match requested size"
        )

    @mock_test(
        orkg.fields.get_hierarchy, return_value=Fabricator.resources.success_get(size=4)
    )
    def test_get_hierarchy(self, *args):
        field_id = "R397"
        res = self.orkg.fields.get_hierarchy(field_id)
        self.assertTrue(res.succeeded, "Request failed")
        self.assertGreaterEqual(len(res.content), 1, "Content is empty")

    @mock_test(orkg.fields.get_hierarchy, return_value=Fabricator.fail())
    def test_get_hierarchy_nonfield(self, *args):
        field_id = "R281058"  # not research field
        res = self.orkg.fields.get_hierarchy(field_id)
        self.assertFalse(res.succeeded, "Request succeeded??")

    @mock_test(
        orkg.fields.get_stats,
        side_effect=[
            Fabricator.rf_stats.success_get(id="R42"),
            Fabricator.rf_stats.success_get_from_cache(_id="R42"),
        ],
    )
    def test_get_stats(self, *args):
        field_id = "R194"
        res = self.orkg.fields.get_stats(field_id)
        total_wo_subfields = res.content["total"]
        self.assertTrue(res.succeeded, "Request failed")
        self.assertEqual(
            len(res.content), 4, "Content format does not match stats output"
        )  # contains id, papers, comparisons, total
        res = self.orkg.fields.get_stats(field_id, include_subfields=True)
        total_with_subfields = res.content["total"]
        self.assertGreaterEqual(
            total_with_subfields,
            total_wo_subfields,
            "Total with subfields less than without subfields",
        )

    @mock_test(orkg.fields.get_stats, return_value=Fabricator.fail())
    def test_get_stats_nonfield(self, *args):
        field_id = "R281058"  # not research field
        res = self.orkg.fields.get_stats(field_id)
        self.assertFalse(res.succeeded, "Request succeeded??")
