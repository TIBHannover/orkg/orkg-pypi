from unittest import TestCase

from orkg.utils import NamespacedClient
from tests import Fabricator


class TestNamespacedClient(TestCase):
    def test_call_pageable_with_no_paging_info_should_fail(self) -> None:
        # test unpageable func with different signature
        with self.assertRaises(ValueError):
            NamespacedClient._call_pageable(
                Fabricator.errors.raise_value_error(),
                args={"id": "C1234"},
                params={},
            )

        # test unpageable func with same signature and unpageable response
        with self.assertRaises(ValueError):
            NamespacedClient._call_pageable(
                Fabricator.pageable.unpageable_func, args={}, params={}
            )

    def test_call_pageable_should_succeed(self) -> None:
        content_size = 5
        total_pages = 25

        response = NamespacedClient._call_pageable(
            Fabricator.pageable.pageable_func,
            args={},
            params={"content_size": content_size, "total_pages": total_pages},
        )

        self.assertTrue(response.all_succeeded)
        self.assertEqual(len(response.responses), total_pages)
        self.assertEqual(len(response.content), content_size * total_pages)

    def test_call_pageable_with_limits_should_succeed(self) -> None:
        content_size = 5
        total_pages = 25
        start_page = 5
        end_page = 10

        response = NamespacedClient._call_pageable(
            Fabricator.pageable.pageable_func,
            args={},
            params={"content_size": content_size, "total_pages": total_pages},
            start_page=start_page,
            end_page=end_page,
        )

        self.assertTrue(response.all_succeeded)
        self.assertEqual(len(response.responses), end_page - start_page)
        self.assertEqual(len(response.content), content_size * (end_page - start_page))
