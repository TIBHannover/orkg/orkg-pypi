from unittest.mock import Mock, patch

from undecorated import undecorated

from .fabrication import Fabricator

# A global switch to enable or disable mocking
# this is particularly useful when debugging
# default is True
ENABLE_MOCKING = True


def mock_test(
    target=None,
    attribute=None,
    new_value=None,
    return_value=None,
    **kwargs,
):
    """Decorator to patch a function or object.

    Args:
        target: The target function or object to patch.
        new_value: The new value to replace the target with. If not specified,
            a mock object will be created.
        attribute: The attribute of the target to patch.
        return_value: The fabrication response to the mocked function or object.
        **kwargs: Keyword arguments to pass to the mock.patch() decorator.

    Returns:
        The patched function or object.
    """

    if not ENABLE_MOCKING:
        return lambda x: x

    if target is None:
        return None

    if new_value is None:
        new_value = Mock()

    if attribute is None:
        if not isinstance(target, str):
            target = undecorated(target)

        if not isinstance(target, str):
            target_str = f"{target.__module__}.{target.__qualname__}"
        else:
            target_str = target
        return patch(
            target_str, new_value=new_value, return_value=return_value, **kwargs
        )
    else:
        return patch.object(
            target, attribute, new_value=new_value, return_value=return_value, **kwargs
        )
