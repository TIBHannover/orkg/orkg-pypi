import json
import uuid
from datetime import datetime
from random import randint
from typing import Any, Dict, List, Optional

from faker import Faker
from pydantic import BaseModel

faker = Faker()
db = {}


class CustomEntityEncoder(json.JSONEncoder):
    def default(self, o: Any) -> str:
        if isinstance(o, datetime):
            return o.isoformat()
        if isinstance(o, uuid.UUID):
            return str(o)
        if isinstance(o, ORKGEntity):
            return str(o)
        return json.JSONEncoder.default(self, o)


class ORKGEntity(BaseModel):
    def __str__(self) -> str:
        # return a json representation of the object
        return json.dumps(self.__dict__, cls=CustomEntityEncoder)

    def as_json(self) -> Dict:
        return json.loads(str(self))


class TypeFabricator:
    class Resource(ORKGEntity):
        id: str
        label: str
        classes: List[str]
        shared: int
        featured: bool = False
        unlisted: bool = False
        verified: bool = False
        extraction_method: str = "UNKNOWN"
        _class: str = "resource"
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()
        observatory_id: uuid.UUID = uuid.uuid4()
        organization_id: uuid.UUID = uuid.uuid4()
        formatted_label: Optional[str] = None

    class Class(ORKGEntity):
        id: str
        label: str
        uri: str
        description: Optional[str] = None
        _class: str = "class"
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()

    class Literal(ORKGEntity):
        id: str
        label: str
        datatype: Optional[str] = "xsd:string"
        _class: str = "literal"
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()

    class ProblemPaperPair(ORKGEntity):
        problem: "TypeFabricator.Resource"
        papers: int

    class Predicate(ORKGEntity):
        id: str
        label: str
        description: Optional[str] = "xsd:string"
        _class: str = "predicate"
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()

    class Statement(ORKGEntity):
        id: str
        subject: ORKGEntity
        predicate: ORKGEntity
        object: ORKGEntity
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()

    class OrkgList(ORKGEntity):
        id: str
        label: str
        elements: List[str]
        _class: str = "list"
        created_at: datetime = datetime.now()
        created_by: uuid.UUID = uuid.uuid4()

    class ResearchFieldStats(ORKGEntity):
        id: str
        papers: int
        comparisons: int
        total: int

    @classmethod
    def fake_resource(cls, **kwargs) -> Dict[str, Any]:
        resource = cls.Resource(
            id=f"R{randint(1421, 654782)}",
            label=faker.name(),
            classes=[faker.word() for _ in range(3)],
            shared=randint(0, 100),
            featured=bool(randint(0, 1)),
            unlisted=bool(randint(0, 1)),
            verified=bool(randint(0, 1)),
            formatted_label=faker.name(),
        )
        for k, v in kwargs.items():
            if k in resource.__dict__:
                resource.__setattr__(k, v)
        # Add to local cache
        db[resource.id] = resource.as_json()
        db[resource.label] = resource.as_json()
        return resource.as_json()

    @classmethod
    def fake_class(cls, **kwargs) -> Dict[str, Any]:
        clazz = cls.Class(
            id=f"C{randint(21, 65472)}",
            label=faker.word(),
            uri=str(faker.uri()),
        )
        for k, v in kwargs.items():
            if k in clazz.__dict__:
                clazz.__setattr__(k, v)
        # Add to local cache
        db[clazz.id] = clazz.as_json()
        db[clazz.label] = clazz.as_json()
        return clazz.as_json()

    @classmethod
    def fake_literal(cls, **kwargs) -> Dict[str, Any]:
        literal = cls.Literal(
            id=f"L{randint(210, 165472)}",
            label=faker.word(),
        )
        for k, v in kwargs.items():
            if k in literal.__dict__:
                literal.__setattr__(k, v)
        # Add to local cache
        db[literal.id] = literal.as_json()
        return literal.as_json()

    @classmethod
    def fake_predicate(cls, **kwargs) -> Dict[str, Any]:
        predicate = cls.Predicate(
            id=f"P{randint(210, 165472)}",
            label=faker.word(),
            description=faker.sentence(),
        )
        for k, v in kwargs.items():
            if k in predicate.__dict__:
                predicate.__setattr__(k, v)
        # Add to local cache
        db[predicate.id] = predicate.as_json()
        db[predicate.label] = predicate.as_json()
        return predicate.as_json()

    @classmethod
    def fake_statement(cls, **kwargs) -> Dict[str, Any]:
        subject = cls.fake_resource()
        predicate = cls.fake_predicate()
        object = cls.fake_resource()
        statement = cls.Statement(
            id=f"S{randint(2100, 2016572)}",
            subject=subject,
            predicate=predicate,
            object=object,
        )
        for k, v in kwargs.items():
            if k in statement.__dict__:
                statement.__setattr__(k, v)
        # Add to local cache
        db[statement.id] = statement.as_json()
        return statement.as_json()

    @classmethod
    def fake_problem_paper_pairs(cls, **kwargs) -> Dict[str, Any]:
        problem_paper_pair = cls.ProblemPaperPair(
            problem=cls.fake_resource(),
            papers=randint(0, 100),
        )
        for k, v in kwargs.items():
            if k in problem_paper_pair.__dict__:
                problem_paper_pair.__setattr__(k, v)
        return problem_paper_pair.as_json()

    @classmethod
    def fake_list(cls, **kwargs) -> Dict[str, Any]:
        lst = cls.OrkgList(
            id=f"R{randint(1421, 654782)}",
            label=faker.catch_phrase(),
            elements=[f"R{randint(1421, 654782)}" for _ in range(5)],
        )
        for k, v in kwargs.items():
            if k in lst.__dict__:
                lst.__setattr__(k, v)
        # Add to local cache
        db[lst.id] = lst.as_json()
        db[lst.label] = lst.as_json()
        return lst.as_json()

    @classmethod
    def fake_rf_stats(cls, **kwargs) -> Dict:
        papers = randint(10, 100)
        comparisons = randint(2, 20)
        stats = cls.ResearchFieldStats(
            id=f"R{randint(1421, 654782)}",
            papers=papers,
            comparisons=comparisons,
            total=papers + comparisons,
        )
        for k, v in kwargs.items():
            if k in stats.__dict__:
                stats.__setattr__(k, v)
        # Add to local cache
        db[stats.id] = stats.as_json()
        return stats.as_json()
