from random import randint
from typing import Any, Dict, List

from faker import Faker

from tests.fabrication.type import TypeFabricator

faker = Faker()


class SamplesFabricator:
    @staticmethod
    def legacy_paper_sample() -> Dict[str, Any]:
        return {
            "predicates": [],
            "paper": {
                "title": "Paper title",
                "doi": "10.1145/3360901.3364435",
                "authors": [
                    {"label": faker.name()},
                ],
                "publicationMonth": "",
                "publicationYear": 2019,
                "publishedIn": "Venue name",
                "researchField": "R11",
                "contributions": [
                    {
                        "name": "Contribution 1",
                        "values": {
                            "P32": [
                                {
                                    "@temp": "_5254e420-ae9a-13ef-1c18-e716b9ea5c2b",
                                    "label": faker.name(),
                                    "classes": ["Problem"],
                                    "values": {},
                                }
                            ],
                            "P1": [
                                {
                                    "@temp": "_851da0a6-7e83-c276-8df3-f66d0a680b30",
                                    "label": faker.word(),
                                    "values": {
                                        "P2": [
                                            {
                                                "text": faker.sentence(),
                                            }
                                        ]
                                    },
                                }
                            ],
                        },
                    }
                ],
            },
        }

    @staticmethod
    def paper_sample(num_authors: int = 2) -> Dict:
        # create new orkg entities to be added to the graph
        resource1 = TypeFabricator.fake_resource()
        resource2 = TypeFabricator.fake_resource()
        predicate = TypeFabricator.fake_predicate()
        literal = TypeFabricator.fake_literal()
        orkg_list = TypeFabricator.fake_list()
        # all temp IDs (entities to be created) must start with #
        resource1["id"] = "#" + resource1["id"]
        resource2["id"] = "#" + resource2["id"]
        predicate["id"] = "#" + predicate["id"]
        literal["id"] = "#" + literal["id"]
        orkg_list["id"] = "#" + orkg_list["id"]
        return {
            "authors": SamplesFabricator.author_sample(num_authors),
            "contents": {
                "contributions": [
                    {
                        "classes": ["C7"],
                        "label": "Contribution 1",
                        "statements": {
                            "P32": [{"id": resource1["id"], "statements": None}],
                            "HAS_EVALUATION": [
                                {"id": resource1["id"], "statements": None},
                                {
                                    "id": resource2["id"],
                                    "statements": {
                                        predicate["id"]: [
                                            {"id": literal["id"], "statements": None},
                                            {"id": orkg_list["id"], "statements": None},
                                        ],
                                        "P32": [
                                            {"id": literal["id"], "statements": None}
                                        ],
                                    },
                                },
                            ],
                        },
                    }
                ],
                "lists": {
                    orkg_list["id"]: {"label": orkg_list["label"], "elements": []}
                },
                "literals": {
                    literal["id"]: {
                        "label": literal["label"],
                        "data_type": literal["datatype"],
                    }
                },
                "predicates": {
                    predicate["id"]: {
                        "label": predicate["label"],
                        "description": predicate["description"],
                    }
                },
                "resources": {
                    resource1["id"]: {
                        "label": resource1["label"],
                        "classes": ["C1", "C2"],
                    },
                    resource2["id"]: {
                        "label": resource2["label"],
                        "classes": ["C3"],
                    },
                },
            },
            "extraction_method": "AUTOMATIC",
            "identifiers": {"doi": [SamplesFabricator.doi_sample()]},
            "observatories": ["1afefdd0-5c09-4c9c-b718-2b35316b56f3"],
            "organizations": ["edc18168-c4ee-4cb8-a98a-136f748e912e"],
            "publication_info": {
                "published_in": "conference",
                "published_month": 5,
                "published_year": 2015,
                "url": "https://www.example.org",
            },
            "research_fields": ["R11"],
            "title": SamplesFabricator.title_sample(),
        }

    @staticmethod
    def doi_sample(make_url: bool = False) -> str:
        """
        Generate a random DOI in the format "10.XXXX/XXXXXX", where XXXX and XXXXXX are random integers or URL with doi.

        :param make_url: A boolean flag indicating whether to format the DOI as a full URL (default is False).
        :return: A randomly generated DOI or a full DOI URL if make_url is True.
        """
        # Generates a fake doi
        doi = f"10.{faker.random_int(min=1000, max=9999)}/{faker.random_int(min=100000, max=999999)}"

        return f"https://doi.org/{doi}" if make_url else doi

    @staticmethod
    def title_sample() -> str:
        # Generates a fake paper title
        return faker.catch_phrase()

    @staticmethod
    def author_sample(num: int) -> List:
        author_list = []
        for i in range(num):
            author_list.append(
                {
                    "homepage": None,
                    "id": None,
                    "identifiers": {
                        "orcid": [
                            "-".join(
                                ["{:04d}".format(randint(0, 9999)) for _ in range(4)]
                            )
                        ]
                    },
                    "name": faker.name(),
                }
            )
        return author_list
