import json
import uuid
import warnings
from random import randint
from typing import Dict, List, Optional, Tuple, Type
from unittest import mock

import networkx as nx
import pandas as pd
from requests import Response

from orkg.out import OrkgResponse
from tests.fabrication.samples import SamplesFabricator
from tests.fabrication.type import TypeFabricator, db


class ResourcesFabricator:
    """
    All the fabricators for the resources endpoint
    """

    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_resource(id=_id))

    @classmethod
    def success_by_id_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_resource(id=_id))

    @classmethod
    def success_get(cls, size: int) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_resource() for _ in range(size)],
        )

    @classmethod
    def success_all(cls) -> mock.Mock:
        return mock.Mock(all_succeeded=True, content=[])

    @classmethod
    def success_add(cls, pageable: Optional[Dict] = None, **kwargs) -> mock.Mock:
        if pageable is not None:
            return mock.Mock(
                succeeded=True,
                content=TypeFabricator.fake_resource(**kwargs),
                page_info={} if pageable else None,
            )
        else:
            return mock.Mock(
                succeeded=True, content=TypeFabricator.fake_resource(**kwargs)
            )

    @classmethod
    def success_update(cls, **kwargs) -> mock.Mock:
        new_resource = TypeFabricator.fake_resource(**kwargs)
        db[new_resource["id"]] = new_resource
        return mock.Mock(succeeded=True, content=new_resource)

    @classmethod
    def success_find_or_add(cls, **kwargs) -> mock.Mock:
        if "id" in kwargs:
            value = db.get(kwargs["id"])
        elif "label" in kwargs:
            value = db.get(kwargs["label"])
        else:
            value = None
        if value is None:
            value = TypeFabricator.fake_resource(**kwargs)
        return mock.Mock(succeeded=True, content=value)

    @classmethod
    def success_delete(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=None)


class ClassesFabricator:
    """
    All the fabricators for the classes endpoint
    """

    @classmethod
    def success_get_all(cls, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True, content=[TypeFabricator.fake_class() for _ in range(size)]
        )

    @classmethod
    def success_get_one_as_list(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content=[TypeFabricator.fake_class(**kwargs)])

    @classmethod
    def success_get_all_by_term(cls, q: str, count: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_class(label=q) for _ in range(count)],
        )

    @classmethod
    def success_get_resources_by_class(cls, class_id: str, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_resource(classes=[class_id]) for _ in range(size)
            ],
        )

    @classmethod
    def success_get_resources_by_class_and_term(
        cls, class_id: str, q: str, count: int = 10
    ) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_resource(classes=[class_id], label=q)
                for _ in range(count)
            ],
        )

    @classmethod
    def success_get_resources_by_class_unpaginated(
        cls, class_id: str, size: int = 10
    ) -> mock.Mock:
        return mock.Mock(
            all_succeeded=True,
            content=[
                TypeFabricator.fake_resource(classes=[class_id]) for _ in range(size)
            ],
        )

    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_class(id=_id))

    @classmethod
    def success_by_id_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_class(id=_id))

    @classmethod
    def success_add(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_class(**kwargs))

    @classmethod
    def success_find_or_add(cls, **kwargs) -> mock.Mock:
        if "id" in kwargs:
            value = db.get(kwargs["id"])
        elif "label" in kwargs:
            value = db.get(kwargs["label"])
        else:
            value = None
        if value is None:
            value = TypeFabricator.fake_class(**kwargs)
        return mock.Mock(succeeded=True, content=value)

    @classmethod
    def success_update(cls, **kwargs) -> mock.Mock:
        new_class = TypeFabricator.fake_class(**kwargs)
        db[new_class["id"]] = new_class
        return mock.Mock(succeeded=True, content=new_class)


class ORKGFabricator:
    @classmethod
    def dummy_token(cls) -> str:
        return "dummy_token"

    @classmethod
    def dummy_class(cls) -> mock.MagicMock:
        return mock.MagicMock()


class ResearchProblemFabricator:
    @classmethod
    def success_get_problem_paper_pair_in_field(cls, **kwargs):
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_problem_paper_pairs(**kwargs)],
        )

    @classmethod
    def success_get_problem_paper_pairs_in_field(cls, size: int = 10, **kwargs):
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_problem_paper_pairs(**kwargs) for _ in range(size)
            ],
        )


class LiteralsFabricator:
    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_literal(id=_id))

    @classmethod
    def success_get_all(cls, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_literal() for _ in range(size)],
        )

    @classmethod
    def success_get_all_by_term(cls, q: str, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_literal(label=q) for _ in range(size)],
        )

    @classmethod
    def success_add(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_literal(**kwargs))

    @classmethod
    def success_update(cls, **kwargs) -> mock.Mock:
        new_literal = TypeFabricator.fake_literal(**kwargs)
        db[new_literal["id"]] = new_literal
        return mock.Mock(succeeded=True, content=new_literal)

    @classmethod
    def success_by_id_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_literal(id=_id))


class ListsFabricator:
    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_list(id=_id))

    @classmethod
    def success_add(cls, **kwargs):
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_list(**kwargs))

    @classmethod
    def success_update(cls, **kwargs):
        new_list = TypeFabricator.fake_list(**kwargs)
        db[new_list["id"]] = new_list
        return mock.Mock(succeeded=True, content=new_list)

    @classmethod
    def success_by_id_from_cache(cls, _id: str):
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_list(id=_id))


class PredicatesFabricator:
    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_predicate(id=_id))

    @classmethod
    def success_get(cls, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_predicate() for _ in range(size)],
        )

    @classmethod
    def success_add(cls, **kwargs) -> mock.Mock:
        return mock.Mock(
            succeeded=True, content=TypeFabricator.fake_predicate(**kwargs)
        )

    @classmethod
    def success_find_or_add(cls, **kwargs) -> mock.Mock:
        if "id" in kwargs:
            value = db.get(kwargs["id"])
        elif "label" in kwargs:
            value = db.get(kwargs["label"])
        else:
            value = None
        if value is None:
            value = TypeFabricator.fake_predicate(**kwargs)
        return mock.Mock(succeeded=True, content=value)

    @classmethod
    def success_update(cls, **kwargs) -> mock.Mock:
        new_predicate = TypeFabricator.fake_predicate(**kwargs)
        db[new_predicate["id"]] = new_predicate
        return mock.Mock(succeeded=True, content=new_predicate)

    @classmethod
    def success_by_id_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_predicate(id=_id))


class StatementsFabricator:
    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_statement(id=_id))

    @classmethod
    def success_get(cls, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[TypeFabricator.fake_statement() for _ in range(size)],
        )

    @classmethod
    def success_get_all(cls, size: int = 10) -> mock.Mock:
        return mock.Mock(
            all_succeeded=True,
            responses=[TypeFabricator.fake_statement() for _ in range(size)],
        )

    @classmethod
    def success_get_by_subject(cls, subject_id: str, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_statement(
                    subject=TypeFabricator.fake_resource(id=subject_id)
                )
                for _ in range(size)
            ],
        )

    @classmethod
    def success_get_by_predicate(cls, predicate_id: str, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_statement(
                    predicate=TypeFabricator.fake_predicate(id=predicate_id)
                )
                for _ in range(size)
            ],
        )

    @classmethod
    def success_get_by_object(cls, object_id: str, size: int = 10) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_statement(
                    object=TypeFabricator.fake_resource(id=object_id),
                )
                for _ in range(size)
            ],
        )

    @classmethod
    def success_get_by_object_and_predicate(
        cls, object_id: str, predicate_id: str, size: int = 10
    ) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_statement(
                    object=TypeFabricator.fake_resource(id=object_id),
                    predicate=TypeFabricator.fake_predicate(id=predicate_id),
                )
                for _ in range(size)
            ],
        )

    @classmethod
    def success_get_by_subject_and_predicate(
        cls, subject_id: str, predicate_id: str, size: int = 10
    ):
        return mock.Mock(
            succeeded=True,
            content=[
                TypeFabricator.fake_statement(
                    subject=TypeFabricator.fake_resource(id=subject_id),
                    predicate=TypeFabricator.fake_predicate(id=predicate_id),
                )
                for _ in range(size)
            ],
        )

    @classmethod
    def success_add_ids(
        cls, subject_id: str, predicate_id: str, object_id: str
    ) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=TypeFabricator.fake_statement(
                subject=TypeFabricator.fake_resource(id=subject_id),
                predicate=TypeFabricator.fake_predicate(id=predicate_id),
                object=TypeFabricator.fake_literal(id=object_id),
            ),
        )

    @classmethod
    def success_update_predicate(cls, predicate_id: str, **kwargs) -> mock.Mock:
        new_statement = TypeFabricator.fake_statement(
            predicate=TypeFabricator.fake_predicate(id=predicate_id), **kwargs
        )
        db[new_statement["id"]] = new_statement
        return mock.Mock(succeeded=True, content=new_statement)

    @classmethod
    def success_by_id_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            return mock.Mock(succeeded=True, content=db[_id])
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_statement(id=_id))


class ContributionsFabricator:
    @classmethod
    def success_comparison_response(cls, **kwargs) -> mock.Mock:
        return mock.Mock(
            succeeded=True, content={"payload": {"comparison": {**kwargs}}}
        )

    @classmethod
    def success_comparison_csv_response(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content="X,Y,Z\n1,2,3\n4,5,6\n")

    @classmethod
    def dataframe_with_metadata(cls) -> Tuple[pd.DataFrame, pd.DataFrame]:
        # Creating the data DataFrame
        data_df = pd.DataFrame({"R34499_data": [1, 2, 3], "R34504_data": [4, 5, 6]})
        # Creating the metadata DataFrame
        meta_df = pd.DataFrame(
            {"R34499_data": ["R34499", "R34504"], "R34504_data": ["P123", "P456"]},
            index=["contribution id", "paper id"],
        )
        return data_df, meta_df


class PageableFabricator:
    @classmethod
    def pageable_func(cls, *args, params: Optional[Dict] = None) -> OrkgResponse:
        base_response = cls.response(True, **params)
        return OrkgResponse(
            client=None,
            response=base_response,
            status_code=str(base_response.status_code),
            content=base_response.json(),
            url="test_url",
            paged=True,
        )

    @classmethod
    def unpageable_func(cls, *args, params: Optional[Dict] = None) -> OrkgResponse:
        base_response = cls.response(False, **params)
        return OrkgResponse(
            client=None,
            response=base_response,
            status_code=str(base_response.status_code),
            content=base_response.json(),
            url="test_url",
            paged=True,
        )

    @classmethod
    def response(
        cls,
        pageable: bool = True,
        status_code: int = 200,
        content_size: int = 2,
        total_pages: int = 10,
        page=0,
    ) -> Response:
        _content = (
            {
                "content": [{"id": "0"}] * content_size,
                "pageable": {},
                "totalPages": total_pages,
            }
            if pageable
            else {
                "content": [{"id": "0"}],
            }
        )

        response = Response()
        response.__setattr__("status_code", status_code)
        response.__setattr__("_content", json.dumps(_content).encode("utf-8"))

        return response


class ErrorFabricator:
    @classmethod
    def raise_value_error(cls):
        raise cls.raise_error(ValueError)

    @classmethod
    def raise_error(cls, error: Type[Exception]):
        raise error


class JsonFabricator:
    @classmethod
    def success_get(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content=kwargs)


class ResearchFieldStatsFabricator:
    @classmethod
    def success_get(cls, **kwargs) -> mock.Mock:
        return mock.Mock(succeeded=True, content=TypeFabricator.fake_rf_stats(**kwargs))

    @classmethod
    def success_get_from_cache(cls, _id: str) -> mock.Mock:
        if _id in db:
            doubled_stats = {
                k: v * 2 if type(v) is int else v for (k, v) in db[_id].items()
            }
            return mock.Mock(succeeded=True, content=doubled_stats)
        else:
            return mock.Mock(
                succeeded=True, content=TypeFabricator.fake_rf_stats(id=_id)
            )


class PapersFabricator:
    @classmethod
    def success_by_id(cls, _id: str) -> mock.Mock:
        return mock.Mock(
            succeeded=True, content={**SamplesFabricator.paper_sample(), "id": _id}
        )

    @classmethod
    def success_get(cls, size: int) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[
                {**SamplesFabricator.paper_sample(), "id": f"R{randint(1421, 654782)}"}
                for _ in range(size)
            ],
        )

    @classmethod
    def success_all(cls) -> mock.Mock:
        return mock.Mock(all_succeeded=True, content=[])

    @classmethod
    def success_get_contributors(cls):
        return mock.Mock(
            succeeded=True, content=[str(uuid.uuid4()) for _ in range(randint(2, 10))]
        )

    @classmethod
    def success_add(cls) -> mock.Mock:
        return mock.Mock(
            succeeded=True,
            content=[],
            url=f"https://sandbox.orkg.org/api/papers/R{randint(1421, 654782)}",
        )


class Fabricator:
    resources = ResourcesFabricator
    classes = ClassesFabricator
    predicates = PredicatesFabricator
    literals = LiteralsFabricator
    lists = ListsFabricator
    statements = StatementsFabricator
    samples = SamplesFabricator
    orkg = ORKGFabricator
    contributions = ContributionsFabricator
    pageable = PageableFabricator
    errors = ErrorFabricator
    problems = ResearchProblemFabricator
    jsons = JsonFabricator
    rf_stats = ResearchFieldStatsFabricator
    papers = PapersFabricator

    @classmethod
    def success_exists(cls, _id: str = "R0") -> mock.Mock:
        return mock.Mock(succeeded=True, content={"id": _id})

    @classmethod
    def success(cls) -> mock.Mock:
        return mock.Mock(succeeded=True)

    @classmethod
    def fail(cls) -> mock.Mock:
        return mock.Mock(succeeded=False)

    @classmethod
    def success_all(cls) -> mock.Mock:
        return mock.Mock(all_succeeded=True)

    @classmethod
    def success_pageable(cls) -> mock.Mock:
        return mock.Mock(succeeded=True, page_info={}, content={})

    @classmethod
    def fail_pageable(cls, warning: Type[Warning] = None) -> mock.Mock:
        if warning:
            warnings.warn("a warning", warning)
        return mock.Mock(succeeded=False, page_info=None, content={})

    @classmethod
    def boolean_false(cls) -> bool:
        return False

    @classmethod
    def boolean_true(cls) -> bool:
        return True

    @classmethod
    def dataframe(cls) -> pd.DataFrame:
        return pd.DataFrame()

    @classmethod
    def empty_subgraph(cls) -> Tuple[List, List, List]:
        return [], [], []

    @classmethod
    def nx_graph(cls) -> nx.DiGraph:
        graph = nx.DiGraph()
        # Create 4 nodes
        graph.add_nodes_from([1, 2, 3, 4])
        # Create 3 edges
        graph.add_edges_from([(1, 2), (1, 3), (2, 4)])
        return graph
