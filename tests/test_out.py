from unittest import TestCase

from orkg.out import OrkgResponse, OrkgUnpaginatedResponse
from tests import Fabricator


class TestOrkgUnpaginatedResponse(TestCase):
    def test_content_is_extended(self) -> None:
        status_code = 200
        content_size = 4
        base_response = Fabricator.pageable.response(
            pageable=True, status_code=status_code, content_size=content_size
        )
        responses = [
            OrkgResponse(
                client=None,
                response=base_response,
                status_code=None,
                content=None,
                url="test_url",
                paged=True,
            ),
            OrkgResponse(
                client=None,
                response=base_response,
                status_code=None,
                content=None,
                url="test_url",
                paged=True,
            ),
        ]

        response = OrkgUnpaginatedResponse(responses=responses)
        self.assertTrue(len(response.responses) == len(responses))
        self.assertTrue(response.all_succeeded)
        self.assertTrue(len(response.content) == content_size * len(response.responses))
