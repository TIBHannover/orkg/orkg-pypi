class AuthenticationRequiredError(RuntimeError):
    """
    Error class for any authentication related issues
    """

    ...
