# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Fetching, listing, creating, updating, and publishing papers with the new papers endpoint ([#56](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/56))

### Changed
- Custom error now alerts users when they call non-GET endpoints without passing credentials ([#76](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/76))

## [[0.21.0]](https://pypi.org/project/orkg/0.21.0/) - 2024-03-14
### Added
- Unpaginated calls documentation ([#48](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/48))
- Multi-class filter on resources via `orkg.resources.get(include="C1,C2")` ([#85](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/85))

### Fixed
- Logging statements bug when using ipython interpreter ([#80](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/80))
- CSVW incomplete dataframe due to deduplication ([#84](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/84))

### Deprecated
- Filtering resource by class in `orkg.classes.get_resource_by_class` ([#85](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/85))

### Changed
- Statement endpoints now use the new backend controller endpoints ([#82](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/82))

## [[0.20.1]](https://pypi.org/project/orkg/0.20.1/) - 2024-02-06
### Added
- Setting `extraction_method` for resources on add or update ([!73](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/73))

## [[0.20.0]](https://pypi.org/project/orkg/0.20.0/) - 2024-01-15
### Added
- Create additional research fields endpoints in the fields, problems, papers, and comparisons clients ([#71](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/71))

### Changed
- Move calling of handle_sort_params ([9350f47c](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/67/diffs?commit_id=9350f47c5c3f24d5b7cddd986875b23cbb1df2f1#00ce2d91ea452d7d355777bd3e2ee8ede0e3c525))

### Fixed
- NestedTemplate list is not processed correctly ([#79](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/79))

## [[0.19.4]](https://pypi.org/project/orkg/0.19.4/) - 2023-12-20
### Added
- `slow_mode` parameter for harvesters to get around backend timeouts of large paper payloads ([!70](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/70))

## [[0.19.3]](https://pypi.org/project/orkg/0.19.3/) - 2023-12-15
### Fixed
- DOI harvester mixes up the metadata of the paper and the supplementary data ([#78](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/78))

## [[0.19.2]](https://pypi.org/project/orkg/0.19.2/) - 2023-12-14
### Added
- Harvesting DOIs now lookup its own supplementary data ([#77](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/77))

## [[0.19.1]](https://pypi.org/project/orkg/0.19.1/) - 2023-12-14
### Fixed
- Bug with `orkg.harvesters.directory_harvest()` of 400 Bad Request ([#75](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/75))

## [[0.19.0]](https://pypi.org/project/orkg/0.19.0/) - 2023-12-06
### Added
- Finding papers via DOI or title via `orkg.papers.by_doi()` ([#50](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/50))
- Fetching problems in a particular research field via `orkg.problems.in_research_field()` ([#38](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/38)).
- Client for fetching, adding, and updating Lists via `orkg.lists` ([#62](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/62))
- Listing research fields with benchmarks via `orkg.fields` ([#39](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/39))
- Missing parameters of `orkg.harvesters.directory_harvest()` ([#67](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/67))
- Documentation for ingesting dataframes via templates ([02873db1](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/62/diffs?commit_id=02873db17e52aaf12b9a6e190bc70d44be0cef46))
- Activate logging in the package ([#1](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/1))

### Changed
- Update typing hints throughout the package ([#45](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/45))
- Using the new SimComp specifications and API ([#29](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/29))

### Fixed
- `datetime.date` is not JSON serializable in templates ([#69](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/69))

## [[0.18.2]](https://pypi.org/project/orkg/0.18.2/) - 2023-10-26
### Added
- Tox and CI testing of multiple python versions ([#60](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/60)).
- CI pipeline to check for formatting issues ([!58](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/58)).
- Directory harvesting accepts the publication month as a parameter ([!60](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/60)).

## [[0.18.1]](https://pypi.org/project/orkg/0.18.1/) - 2023-10-23
### Fixed
- OID parsing from JSON-LD format problems ([#58](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/58)).

## [[0.18.0]](https://pypi.org/project/orkg/0.18.0/) - 2023-08-29
### Added
- Add changelog ([#35](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/35)).
- Ping host functionality `orkg.ping()` ([#36](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/36)).
- Cast `OrkgResource` to `Type` ([#41](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/41)).
- Type client property of `NamespacedClient` ([#49](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/49)).
- Add directory harvesting for template JSON-LD output ([#53](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/53)).

### Fixed
- Decimal datatype is supported as DF within templates ([#42](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/42)).
- Improve backend client tests ([#37](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/37)).
- Fix IDE typing hints for `ORKG` base class ([!46](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/merge_requests/46)).
- Simplify code for `add_csv` and `compare_dataframe` functions ([#34](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/34)).
- Importing tables via templated instances was empty ([be97df05](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/commit/be97df05b2bf79cdd66708ec6af9f54c025198b6#5b8b17dd581083e42da539995a460397ab07cbb5_0_60)).

## [[0.17.0]](https://pypi.org/project/orkg/0.17.0/) - 2023-08-03

### Added
- Mock tests ([#28](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/28)).
- Improve host and simcomp host assignment ([#33](https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/issues/33)).
- Add [contribution guidelines](CONTRIBUTING.md).

### Changed
- Switch minimum python version to 3.8.
- Default host of the package set to Sandbox.
