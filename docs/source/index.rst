Welcome to ORKG's python package documentation!
===============================================

ORKG python package is a simple API wrapper for the ORKG's API.

Start using it by typing these few words:

.. code-block:: bash

    $ pip install orkg

.. image:: orkg_loves_python.jpg
    :width: 400
    :alt: ORKG loves Python
    :align: center


Breaking Changes
-----------------
* Starting with python package 0.19.0, the package only works with new SimComp API.

* Starting with python package 0.17.0, the package only works with Python 3.8+.

* Starting with python package 0.17.0, the default host is now sandbox (previously it was localhost).

* Starting with python package 0.16.3, the CSV import functionality is deprecated. You can still add papers by CSV import using the frontend.

* Starting with python package 0.15.0, the pagination parameter is renamed. Check Parameter Name Changes below.

* Starting with python package 0.12.2, comparison dataframes are typed and multiple values per cell are replaced by a list of values.

* Starting with python package 0.11.0, all paginated calls have new pagination parameters. Check Parameter Name Changes below.


Parameter Name Changes
********************
List of parameter name changes.

    +----------------+----------------+----------------+-----------------------------+
    | Old param name | New param name | From version   | Comments                    |
    +================+================+================+=============================+
    | items          | size           | 0.11.0         | Paginated calls affected    |
    +----------------+----------------+----------------+-----------------------------+
    | sortBy         | sort           | 0.11.0         | Paginated calls affected    |
    +----------------+----------------+----------------+-----------------------------+
    | pageable       | page_info      | 0.15.0         | Paginated response affected |
    +----------------+----------------+----------------+-----------------------------+

    The rest of the params stay the same.

Deprecated Features
********************
List of deprecated or removed features.

    +-----------------------+------------------------------------------+----------------+----------------+
    | Feature               | Function name                            | Deprecated     | Removed        |
    +=======================+==========================================+================+================+
    | CSV import            | ``orkg.papers.add_csv()``                | 0.16.3         | TBA            |
    +-----------------------+------------------------------------------+----------------+----------------+
    | Resources by class    | ``orkg.classes.get_resource_by_class()`` | 0.21.0         | TBA            |
    +-----------------------+------------------------------------------+----------------+----------------+


.. toctree::
    :maxdepth: 3

    introduction
    installation
    client
    graph

..
    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
