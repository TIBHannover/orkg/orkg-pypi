Introduction
============
This package is just a wrapper for the ORKG's functionality, it provides an easy access to the API.

What is ORKG
^^^^^^^^^^^^
We build the next generation digital libraries for semantic scientific knowledge communicated in scholarly literature. We focus on the communicated content rather than the context e.g., people and institutions in which scientific knowledge is communicated, and the content is semantic i.e., machine interpretable.

Scientific knowledge continues to be confined to the document, seemingly inseparable from the medium as hieroglyphs carved in stone. The global scientific knowledge base is little more than a collection of documents. It is written by humans for humans, and we have done so for a long time. This makes perfect sense, after all it is people that make up the audience, and researchers in particular.

Yet, with the monumental progress in information technologies over the more recent decades, one may wonder why it is that the scientific knowledge communicated in scholarly literature remains largely inaccessible to machines. Surely it would be useful if some of that knowledge is more available to automated processing.

The Open Research Knowledge Graph project is working on answers and solutions. The recently initiated TIB coordinated project is open to the community and actively engages research infrastructures and research communities in the development of technologies and use cases for open graphs about research knowledge.

For more information about the ORKG, please check the `project page <https://projects.tib.eu/orkg/>`_, and the `application page <https://orkg.org>`_.

Who can use orkg package
^^^^^^^^^^^^^^^^^^^^^^^^
Well, the short answer is anybody. The package is designed to be minimalistic, simple knowledge of python and JSON suffices for you to know and understand how the package works.

You can use the ORKG package to add/edit/list data from any instance of the open research knowledge graph. It can be easily integrated into data science workflows, it can be used to fetch data for analysis and visualizations.

The sky is your limit :)
