ORKG Predicates
===============
Entities in the ORKG can be of type *Predicate*, in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *predicates* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the predicates manager directly to do the following:

Getting predicates by ID
^^^^^^^^^^^^^^^^^^^^^^^^
You can get certain predicate given that you know their ID value

.. code-block:: python

    ### Fetch predicate by id
    orkg.predicates.by_id(id='P1')
    >>> (Success)
    {
       "id":"P1",
       "label":"yields",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"predicate"
    }

Getting predicates by lookup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup a predicate by the label of it. A variety of parameter can be passed to specify an exact match, number of results to retrieve, and what order you need them in.

.. code-block:: python

    ### Fetch a list of predicates
    # All parameters are optional
    # q: to search for a label containing
    # exact: to condition the search to as is
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    orkg.predicates.get(q='yield',exact=False,size=30,sort='label',desc=True)
    >>> (Success)
    [
       {
          "id":"P1",
          "label":"yields",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       ...
    ]

Getting predicates by lookup without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup a predicate by the label of it. A variety of parameter can be passed to specify an exact match, number of results to retrieve, and what order you need them in without pagination.

.. code-block:: python

    ### Fetch a list of predicates
    # All parameters are optional
    # q: to search for a label containing
    # exact: to condition the search to as is
    # page: the page number (optional)
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    # start_page: page to start from. Defaults to 0 (optional)
    # end_page: page to stop at. Defaults to -1 meaning non-stop (optional)
    orkg.predicates.get_unpaginated(q='addresses',exact=False,size=30,sort='label',desc=True)
    >>> (Success)
    [
       {
          "id":"P0",
          "label":"addresses",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       ...
    ]

Adding a new predicate
^^^^^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new predicates in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added predicates will be credited to your user.*

.. code-block:: python

    ### Adds a predicate to the graph
    # id: the id of the predicate (optional)
    # label: the label of the predicate
    orkg.predicates.add(id='Custom_ID', label='new predicate')
    >>> (Success)
    {
       "id":"Custom_ID",
       "label":"new predicate",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"predicate"
    }

Updating an existing predicate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing predicates in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Updates a predicate
    # id (mandatory): the id of the predicate to change
    # label: the new label of the predicate
    orkg.predicates.update(id='Custom_ID', label='the newest predicate!')
    >>> (Success)
    {
       "id":"Custom_ID",
       "label":"the newest predicate!",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"predicate"
    }

Check if a predicate exist
^^^^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of predicates before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if predicate exists
    # id: the id of the predicate
    # returns a bool
    orkg.predicates.exists(id='P112313')
    >>> False
