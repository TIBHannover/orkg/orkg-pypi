ORKG Lists
=============
Lists represent an ordered sequence of objects in the ORKG.  Lists are always part of a statement in which the subject is the list label, the predicate is "has list element", and the object is the list of elements.  The list itself is a resource with the class type "List" and it may contain resources, classes, predicates, or literals as the elements.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the lists manager directly to do the following:

Get lists by ID
^^^^^^^^^^^^^^^^^^^^^^^
Returns the label, element IDs, and metadata of a list.

Parameters:

* :code:`list_id` : the id of the list to lookup (required)

.. code-block:: python

    ### Fetch list by id
    orkg.lists.by_id("R315000")
    >>> (Success)
    {   'id': 'R315000',
        'label': 'authors list',
        'elements': ['R67818', 'R74265'],
        'created_at': '2023-09-08T07:45:39.100683+02:00',
        'created_by': '00000000-0000-0000-0000-000000000000',
        '_class': 'list'
    }

Get list elements
^^^^^^^^^^^^^^^^^^^^^^^
Returns the full representations of the list elements.

Parameters:

* :code:`list_id` : the id of the list to lookup (required)
* :code:`page` : the page number (optional)
* :code:`size` : number of items per page (optional)
* :code:`sort` : key to sort on (optional)
* :code:`desc` : true/false to sort desc (optional)

.. code-block:: python

    ### Fetch list elements
    orkg.lists.get_elements("R338045", size=2)
    >>> (Success)
    [
        {   'id': 'L638050',
            'label': 'John Smith',
            'datatype': 'xsd:string',
            'created_at': '2023-10-26T11:07:03.30482485+02:00',
            'created_by': 'da70017b-ddd8-41c2-9c08-b222719c57d8',
            '_class': 'literal'
        },
        {   'id': 'L638051',
            'label': 'Hong Gildong',
            'datatype': 'xsd:string',
            'created_at': '2023-10-26T11:07:03.314806701+02:00',
            'created_by': 'da70017b-ddd8-41c2-9c08-b222719c57d8',
            '_class': 'literal'
        }
    ]


Create a new list
^^^^^^^^^^^^^^^^^^^^
Creates a list with the specified parameters and returns the new list object.

Parameters:

* :code:`label` : the label of the list (required)
* :code:`elements` : list of IDs for the elements of the list (required)

*Note: if you have your credentials entered in the ORKG instance creation all newly added lists will be credited to your user.*

.. code-block:: python

    ### Add a list
    orkg.lists.add(label="my list", elements=["R1", "R2"])
    >>> (Success)
    {   'id': 'R338165',
        'label': 'my list',
        'elements': ['R1', 'R2'],
        'created_at': '2023-11-04T19:00:44.729778758+01:00',
        'created_by': '00000000-0000-0000-0000-000000000000',
        '_class': 'list'
    }

Update an existing list
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Updates an existing list by replacing the label and/or the elements with new values.  Does **not** return the updated list object.

Parameters:

* :code:`list_id` : the id of the list to update (required)
* :code:`label` : the new label of the list (optional)
* :code:`elements` : list of IDs for the new elements of the list (optional)

.. code-block:: python

    ### Update a list
    orkg.lists.update(list_id="R338165", label="new label", elements=["R3", "R4"])
    >>> (Success)
