ORKG JSON objects
=========================

.. warning::

    Should be used by power-users only. This endpoint could have unaccounted consequences.

The SimComp service has an endpoint to store json objects to enable other services performing persistence features.

You need to have the `simcomp_host` defined to be able to use this client. If you instantiate the ORKG client using a web address or host environment name (``Hosts.PRODUCTION``, ``Hosts.SANDBOX``, or ``Hosts.INCUBATING``), the corresponding `simcomp_host` is automatically assigned.

However, if you are running the ORKG locally then you need to specify `simcomp_host`.  See :ref:`Host <host>` for details.


We can access the json client directly to do the following:

Save JSON objects
^^^^^^^^^^^^^^^^^^^^^^^^^
With this simple request you can store arbitrary json objects in the SimComp service. This is used for Visualization, Reviews, Comparisons, etc.

.. code-block:: python

    from orkg import ORKG, ThingType
    # Config parameter is optional and used only for the COMPARISON/DRAFT_COMPARISON types
    # Possible values for thing_type are: UNKNOWN, COMPARISON, DIAGRAM, VISUALIZATION, DRAFT_COMPARISON, LIST, REVIEW, QUALITY_REVIEW, PAPER_VERSION, ANY
    orkg.json.save_json(thing_key="RRR22", thing_type=ThingType.ANY, data={"test": "test"}, config={})
    >>> (Success)
    {
        "success":true
    }

Load JSON objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also retrieve json values by the key and the type.

.. code-block:: python

    from orkg import ORKG, ThingType

    # All parameters are required
    orkg.json.get_json(thing_key="R307226", thing_type=ThingType.COMPARISON)
    >>> (Success)
    {
       "timestamp":"2023-11-24T15:05:30.221460",
       "uuid":"1231f1b1-cae3-4d00-a648-e52ef85bb42e",
       "payload":{
          "thing":{
             "id":"28650fc0-da87-4c16-84ae-224e1cd2cf12",
             "created_at":"2023-07-04T08:57:18.175301Z",
             "updated_at":"2023-07-04T08:57:18.175312Z",
             "thing_type":"COMPARISON",
             "thing_key":"R307226",
             "config": {
                "predicates":[
                    "location",
                    "research problem",
                ],
                "contributions":[
                   "R288053",
                   "R288056"
                ],
                "transpose":false,
                "type":"PATH"
             },
             "data": {
                ......
             }
          }
       }
    }
