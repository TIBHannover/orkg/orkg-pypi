ORKG Papers
===========
One of the main power points of the ORKG is the ability to represent scientific papers in a structured manner. The package also provide an endpoint to add new papers directly using the concepts defined on other components.

A paper technically is a collection of resources, literals, and statements connecting them in a small sub-graph.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the papers manager directly to do the following:

Adding new papers
^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new papers in the ORKG instance you are connected to.  The same method accepts two different input structures: one corresponding to the current endpoint and one corresponding to the old (outdated) endpoint.

*Note: your credentials must be entered in the ORKG instance in order to use this functionality. All newly added papers will be credited to your user.*

Current endpoint
"""""""""""""""""""""""
This is the most up-to-date endpoint with all the currently available features.  If successful, the method returns a ``201 Created`` and the response url will be the newly created paper object.

The method takes one parameter which is a dictionary with the following keys:

* :code:`title` : string of the paper title
* :code:`research_fields` : list of research field resource IDs
* :code:`identifiers` : dictionary
    * :code:`doi` : list containing the string of the DOI (optional)
* :code:`publication_info` : dictionary
    * :code:`published_month` : integer (optional)
    * :code:`published_year` : integer (optional)
    * :code:`published_in` : string of publication venue (optional)
    * :code:`url` : string of the url to the original paper (optional)
* :code:`authors` : list of dictionaries
    * :code:`name` : string of author name
    * :code:`id` : string of author ORKG ID (optional)
    * :code:`homepage` : string of author's homepage (optional)
    * :code:`identifiers` : dictionary of unique identifiers
        * :code:`orcid` : list of ORCIDs of the author (optional)
        * :code:`google_scholar` : list of Google Scholar IDs of the author (optional)
        * :code:`research_gate` : list of ResearchGate IDs of the author (optional)
        * :code:`linked_in` : list of LinkedIn IDs of the author (optional)
        * :code:`wikidata` : list of Wikidata IDs of the author (optional)
        * :code:`web_of_science` : list of Web of Science IDs of the author (optional)
* :code:`contents` : dictionary
    * :code:`resources` : dictionary of resources that need to be created
        * :code:`#___` : ID of new resource, must start with #
            * :code:`label` : string of new resource label
            * :code:`classes` : list of strings of resource ID
    * :code:`literals` : dictionary of literals that need to be created
        * :code:`#___` : ID of new literal, must start with #
            * :code:`label` : string of new literal
            * :code:`data_type` : string of the literal datatype (e.g. xsd:float)
    * :code:`predicates` : dictionary of predicates that need to be created
        * :code:`#___` : ID of new predicate, must start with #
            * :code:`label` : string of new predicate label
            * :code:`description` : string of the predicate description
    * :code:`lists` : dictionary of lists that need to be created
        * :code:`#___` : ID of new list, must start with #
            * :code:`label` : string of new list label
            * :code:`elements` : list of IDs of the elements in the list
    * :code:`contributions` : list of contribution dictionaries
        * :code:`label` : string of the contribution label
        * :code:`classes` : list of strings of resource ID
        * :code:`statements` : dictionary, recursive map of statements contained within the contribution
            * :code:`id` : ID of the object of the statement
* :code:`organizations` : list of IDs of the organizations the paper belongs to
* :code:`observatories` : list of IDs of the observatories the paper belongs to
* :code:`extraction_method` : method used to extract the paper resource; can be one of "UNKNOWN", "MANUAL" or "AUTOMATIC"
* :code:`sdgs` : set of IDs of UN Sustainable Development Goals the paper will be assigned to (optional)

.. code-block:: python

    # First define the paper content as dict (JSON object) (This is a sample of input)
    paper = {
      "authors" : [ {
        "homepage" : null,
        "id" : "R123",
        "identifiers" : null,
        "name" : "Author with id"
      }, {
        "homepage" : null,
        "id" : null,
        "identifiers" : {
          "orcid" : [ "0000-1111-2222-3333" ]
        },
        "name" : "Author with orcid"
      }, {
        "homepage" : null,
        "id" : "R456",
        "identifiers" : {
          "orcid" : [ "1111-2222-3333-4444" ]
        },
        "name" : "Author with id and orcid"
      }, {
        "homepage" : "http://example.org/author",
        "id" : null,
        "identifiers" : null,
        "name" : "Author with homepage"
      }, {
        "homepage" : null,
        "id" : null,
        "identifiers" : null,
        "name" : "Author that just has a name"
      } ],
      "contents" : {
        "contributions" : [ {
          "classes" : [ "C123" ],
          "label" : "Contribution 1",
          "statements" : {
            "P32" : [ {
              "id" : "R3003",
              "statements" : null
            } ],
            "HAS_EVALUATION" : [ {
              "id" : "#temp1",
              "statements" : null
            }, {
              "id" : "R3004",
              "statements" : {
                "#temp3" : [ {
                  "id" : "R3003",
                  "statements" : null
                }, {
                  "id" : "#temp2",
                  "statements" : null
                }, {
                  "id" : "#temp4",
                  "statements" : null
                } ],
                "P32" : [ {
                  "id" : "#temp2",
                  "statements" : null
                } ]
              }
            } ]
          }
        } ],
        "lists" : {
          "#temp4" : {
            "elements" : [ "#temp1", "C123" ],
            "label" : "list"
          }
        },
        "literals" : {
          "#temp2" : {
            "data_type" : "xsd:decimal",
            "label" : "0.1"
          }
        },
        "predicates" : {
          "#temp3" : {
            "description" : "has result",
            "label" : "hasResult"
          }
        },
        "resources" : {
          "#temp1" : {
            "classes" : [ "Result" ],
            "label" : "MOTO"
          }
        }
      },
      "extraction_method" : "MANUAL",
      "identifiers" : {
        "doi" : [ "10.48550/arXiv.2304.05327" ]
      },
      "observatories" : [ "1afefdd0-5c09-4c9c-b718-2b35316b56f3" ],
      "organizations" : [ "edc18168-c4ee-4cb8-a98a-136f748e912e" ],
      "publication_info" : {
        "published_in" : "conference",
        "published_month" : 5,
        "published_year" : 2015,
        "url" : "https://www.example.org"
      },
      "research_fields" : [ "R12" ],
      "sdgs" : [ "SDG_1" ],
      "title" : "Gummy Bears and Shawarma"
    }

    # Then add the content as a paper to the ORKG instance
    res = orkg.papers.add(params=paper)
    <<< res.url
    >>> https://sandbox.orkg.org/api/papers/R364862

Old endpoint
""""""""""""""""""
You can also still use the old papers endpoint to add papers, although this endpoint may become deprecated in the future.  The return value is the newly created paper resource.

The method has one required parameter which is a dictionary with the following keys:

* :code:`predicates` : list of new predicates to be created (optional)
* :code:`paper` : the core paper content
    * :code:`title` : string of the paper title
    * :code:`researchField` : string of research field resource ID
    * :code:`doi` : string of the DOI without the http and the host (optional)
    * :code:`authors` : list of Author objects (optional)
            * :code:`id` : string of resource ID (optional)
            * :code:`label` : string of author name (optional)
            * :code:`orcid` : string of orcid ID (optional)
    * :code:`publicationMonth` : integer (optional)
    * :code:`publicationYear` : integer (optional)
    * :code:`publishedIn` : string of publication venue (optional)
    * :code:`contributions` : list of Contribution objects (optional)
            * :code:`name` : string of contribution name
            * :code:`classes` : list of strings of resource ID (optional)
            * :code:`extraction_method` : string (optional)
            * :code:`observatory_id` : string (optional)
            * :code:`organization_id` : string (optional)
            * :code:`values` : dictionary of statements with predicate ID as key and list of Value objects (see below for keys) as the value (optional)
                * :code:`@id` : string of ORKG entity ID (optional)
                * :code:`@type` : string of type (resource, literal, class, predicate); used with `@id` to disambiguate similar IDs (optional)
                * :code:`text` : string of a literal (optional)
                * :code:`datatype` : string of the literal datatype (e.g. xsd:float); used with `text` (optional)
                * :code:`label` : string of new resource label (optional)
                * :code:`classes` : list of strings of resource ID (optional)
                * :code:`@temp` : string of temporary value to link newly created resources within the paper request, must start with an underscore (optional)
                * :code:`values` : Value object (optional)

There is also an optional parameter:

* :code:`merge_if_exists` : if the paper already exists in the graph, the passed values will be appended to it (default is False)

.. note::
    The old endpoint does not support all of the features in the current version, e.g. you cannot create lists.

.. code-block:: python

    # First define the paper content as dict (JSON object) (This is a sample of input)
    paper = {
       "predicates":[

       ],
       "paper":{
          "title":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
          "doi":"10.1145/3360901.3364435",
          "authors":[
             {
                "label":"Mohamad Yaser Jaradeh"
             },
             {
                "label":"Allard Oelen"
             },
             {
                "label":"Kheir Eddine Farfar"
             },
             {
                "label":"Manuel Prinz"
             },
             {
                "label":"Jennifer D'Souza"
             },
             {
                "label":"Gábor Kismihók"
             },
             {
                "label":"Markus Stocker"
             },
             {
                "label":"Sören Auer"
             }
          ],
          "publicationMonth":"",
          "publicationYear":2019,
          "publishedIn":"Proceedings of the 10th International Conference on Knowledge Capture  - K-CAP '19",
          "researchField":"R11",
          "contributions":[
             {
                "name":"Contribution 1",
                "values":{
                   "P32":[
                      {
                         "@temp":"_5254e420-ae9a-13ef-1c18-e716b9ea5c2b",
                         "label":"Similarity measures",
                         "classes":["Problem"],
                         "values":{

                         }
                      }
                   ],
                   "P3":[
                      {
                         "text":"Find similar research contributions inside the ORKG and suggest them to the user"
                      }
                   ],
                   "P1":[
                      {
                         "@temp":"_851da0a6-7e83-c276-8df3-f66d0a680b30",
                         "label":"TF/iDF",
                         "values":{
                            "P2":[
                               {
                                  "text":"Term frequency, inverse document frequency technique"
                               }
                            ]
                         }
                      }
                   ]
                }
             }
          ]
       }
    }

    # Then add the content as a paper to the ORKG instance
    orkg.papers.add(params=paper)
    >>> (Success)
    {
       "id":"R8186",
       "label":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
       "created_at":"2019-12-19T15:07:02.136+01:00",
       "classes":[
          "Paper"
       ],
       "shared":0,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

    # you can append the contribution into the paper if it exists in the system by specifying the `merge_if_exists` property
    orkg.papers.add(params=paper, merge_if_exists=True)
    >>> (Success)
    {
       "id":"R8186",
       "label":"Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
       "created_at":"2019-12-19T15:07:02.136+01:00",
       "classes":[
          "Paper"
       ],
       "shared":0,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Adding new papers via CSV
^^^^^^^^^^^^^^^^^^^^^^^^^
An easier way to import papers from a csv file containing the details is detailed in this `Wiki page <https://gitlab.com/TIBHannover/orkg/orkg-pypi/-/wikis/Tutorial:-importing-papers-in-CSV-format-into-the-ORKG>`_.

.. warning::
    This functionality is deprecated as of version 0.16.3 and will be removed in a future update! You can still add papers by CSV import using the frontend.

Updating an existing paper
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can edit the metadata of an already created paper. The passed values will replace the existing values.  The return value is an OrkgResponse indicating success or failure of the request (no content).

Parameters:

* :code:`paper_id` : the resource ID of the paper
* :code:`title` : the updated title (optional)
* :code:`research_fields` : the updated list of research fields of the paper (optional)
* :code:`identifiers` : the updated unique identifiers of the paper (optional)
* :code:`publication_info` : the updated publication info of the paper (optional)
* :code:`authors` : the updated list of authors' info of the paper (optional)
* :code:`organizations` : the updated list of IDs of the organizations the paper belongs to (optional)
* :code:`observatories` : the updated list of IDs of the observatories the paper belongs to (optional)
* :code:`sdgs` : the updated set of IDs of UN sustainable development goals the paper will be assigned to (optional)

.. note::
    There are two ways to add a *contribution* to a paper: use the :code:`papers.add()` method with the **old endpoint** input structure and set :code:`merge_if_exists=True`. Alternatively, you can use the :ref:`Contributions client <addContribution>`!

.. code-block:: python

    # Update an existing paper
    orkg.papers.update(paper_id="R350025", title="Gummibären", research_fields=["R11"])
    >>> (Success)

Assigning a DOI to an ORKG paper representation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Publish an existing paper by assigning a DOI to the ORKG paper object and adding publication info. The return value is an OrkgResponse indicating success or failure of the request (no content).  This method will throw an error if you use it on a paper object that already has a DOI.

Parameters:

* :code:`paper_id` : the resource ID of the paper
* :code:`subject` : the subject of the paper
* :code:`description` : the description of the paper
* :code:`authors` : the list of authors' info

.. code-block:: python

    # Publish an existing paper
    orkg.papers.publish(paper_id="R350025", subject="gummy bears", description="Study of confectionery biology exploring the ecological niche and evolutionary adaptations of gummy bears, revealing how these gelatinous delights have thrived in their sugary habitat.", authors=[...])
    >>> (Success)

Getting paper ID by providing paper doi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get paper/papers by providing paper doi

.. code-block:: python

    # Fetch a list of papers by doi
    # You can also use the full doi link (e.g. https://doi.org/10.1108/ijpcc-06-2016-0030)
    orkg.papers.by_doi(doi="10.1108/ijpcc-06-2016-0030")
    >>> (Success)
    [{
        "id": "R3000",
        "title": "A model for contextual data sharing in smartphone applications",
        "research_fields": [
            {
                "id": "R278",
                "label": "Information Science"
            }
        ],
        "identifiers": {
            "doi": "10.1108/ijpcc-06-2016-0030"
        },
        "publication_info": {
            "published_month": 9,
            "published_year": 2016,
            "published_in": "International Journal of Pervasive Computing and Communications",
            "url": null
        },
        "authors": [
            {
                "id": "R67818",
                "name": "Harshvardhan Pandit",
                "identifiers": {
                    "orcid": "0000-0002-5068-3714"
                },
                "homepage": null
            },
            {
                "id": "R74265",
                "name": "Adrian O'Riordan",
                "identifiers": {
                    "orcid": "0000-0002-5264-6044"
                },
                "homepage": null
            }
        ],
        "contributions": [
            {
                "id": "R3005",
                "label": "Contribution 1"
            }
        ],
        "observatories": [
            "00000000-0000-0000-0000-000000000000"
        ],
        "organizations": [
            "00000000-0000-0000-0000-000000000000"
        ],
        "extraction_method": "UNKNOWN",
        "created_at": "2019-01-06T15:04:07.692Z",
        "created_by": "00000000-0000-0000-0000-000000000000",
        "verified": true,
        "visibility": "DEFAULT"
    }]

Getting paper ID by providing paper title
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get paper/papers id by providing the title

.. code-block:: python

    # Fetch a list of papers by title
    orkg.papers.by_title(title="A model for contextual data sharing in smartphone applications")
    >>> (Success)
    [{
        "id": "R3000",
        "title": "A model for contextual data sharing in smartphone applications",
        "research_fields": [
            {
                "id": "R278",
                "label": "Information Science"
            }
        ],
        "identifiers": {
            "doi": "10.1108/ijpcc-06-2016-0030"
        },
        "publication_info": {
            "published_month": 9,
            "published_year": 2016,
            "published_in": "International Journal of Pervasive Computing and Communications",
            "url": null
        },
        "authors": [
            {
                "id": "R67818",
                "name": "Harshvardhan Pandit",
                "identifiers": {
                    "orcid": "0000-0002-5068-3714"
                },
                "homepage": null
            },
            {
                "id": "R74265",
                "name": "Adrian O'Riordan",
                "identifiers": {
                    "orcid": "0000-0002-5264-6044"
                },
                "homepage": null
            }
        ],
        "contributions": [
            {
                "id": "R3005",
                "label": "Contribution 1"
            }
        ],
        "observatories": [
            "00000000-0000-0000-0000-000000000000"
        ],
        "organizations": [
            "00000000-0000-0000-0000-000000000000"
        ],
        "extraction_method": "UNKNOWN",
        "created_at": "2019-01-06T15:04:07.692Z",
        "created_by": "00000000-0000-0000-0000-000000000000",
        "verified": true,
        "visibility": "DEFAULT"
    }]

Getting papers by research field ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of papers from within a particular research field.  You may optionally include papers from subfields as well.  If the response is an empty list, it means the research field has no papers (or that the given ID does not correspond to a resource with the 'ResearchField' class label).

.. code-block:: python

    ### Fetch papers by research field id
    # research_field_id: the id of the research field
    # include_subfields: True/False whether to include papers from subfields, default is False (optional)
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.papers.in_research_field('R132', include_subfields=True, size=3)
    >>> (Success)
    [
       {
          'id': 'R339153',
          'label': 'Ontology-driven weak supervision for clinical entity classification in electronic health records',
          'classes': ['Paper']
       },
       {
          'id': 'R309164',
          'label': 'BN-DRISHTI: Bangla Document Recognition through Instance-level Segmentation of Handwritten Text Images',
          'classes': ['Paper']
      },
      {
          'id': 'R333086',
          'label': 'Management of Hyperglycemia in Type 2 Diabetes: A Patient-Centered Approach',
          'classes': ['Paper']
      }
    ]

Getting papers by ORKG ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can fetch a paper by ORKG ID.

Parameter:

* :code:`id` : string of the paper's ORKG ID

.. code-block:: python

    ### Fetch paper by orkg id
    orkg.papers.by_id('R8186')
    >>> (Success)
     {
        'id': 'R8186',
        'title': 'Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge',
        'research_fields': [{'id': 'R135', 'label': 'Databases/Information Systems'}],
        'identifiers': {'doi': ['10.1145/3360901.3364435']},
        'publication_info': {
            'published_month': 11,
            'published_year': 2019,
            'published_in': {'id': 'R10000', 'label': "Proceedings of the 10th International Conference on Knowledge Capture  - K-CAP '19"},
            'url': None
        },
        'authors': [
            {'id': 'R8187', 'name': 'Mohamad Yaser Jaradeh', 'identifiers': {'orcid': ['0000-0001-8777-2780']}, 'homepage': None},
            {'id': 'R8188', 'name': 'Allard Oelen', 'identifiers': {'orcid': ['0000-0001-9924-9153']}, 'homepage': None},
            {'id': 'R8189', 'name': 'Kheir Eddine Farfar', 'identifiers': {'google_scholar': ['UqED_fMAAAAJ'], 'research_gate': ['Kheir-Eddine-Farfar'], 'linked_in': ['kfarfar'], 'orcid': ['0000-0002-0366-4596']}, 'homepage': 'https://reddine.github.io'},
            {'id': 'R8190', 'name': 'Manuel Prinz', 'identifiers': {'orcid': ['0000-0003-2151-4556']}, 'homepage': None},
            {'id': 'R8191', 'name': 'Jennifer D’Souza', 'identifiers': {'linked_in': ['https://www.linkedin.com/in/jennifer-l-dsouza/'], 'orcid': ['0000-0002-6616-9509']}, 'homepage': None},
            {'id': 'R8192', 'name': 'Gábor Kismihók', 'identifiers': {'orcid': ['0000-0003-3758-5455']}, 'homepage': None},
            {'id': 'R8011', 'name': 'Markus Stocker', 'identifiers': {'orcid': ['0000-0001-5492-3212']}, 'homepage': None},
            {'id': 'R8193', 'name': 'Sören Auer', 'identifiers': {'research_gate': ['https://www.researchgate.net/profile/Soeren-Auer'], 'google_scholar': ['https://scholar.google.de/citations?user=2cpal78AAAAJ&hl=en'], 'linked_in': ['https://www.linkedin.com/in/soerenauer/'], 'orcid': ['0000-0002-0698-2864']}, 'homepage': 'https://tib.eu/auer'}
        ],
        'contributions': [
            {'id': 'R189082', 'label': 'Contribution 1'},
            {'id': 'R8194', 'label': 'Literature Comparison'},
            {'id': 'R8197', 'label': 'Contribution Similarity'},
            {'id': 'R8199', 'label': 'ORKG System'}
        ],
        'sdgs': [],
        'observatories': ['00000000-0000-0000-0000-000000000000'],
        'organizations': ['00000000-0000-0000-0000-000000000000'],
        'extraction_method': 'UNKNOWN',
        'created_at': '2019-12-19T15:07:02.136+01:00',
        'created_by': '00000000-0000-0000-0000-000000000000',
        'verified': True,
        'visibility': 'FEATURED',
        'modifiable': True
    }

Getting papers by lookup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can fetch a list of papers using search filters.

Parameters:

* :code:`title` : string of search term that must be contained in the title of the paper (optional)
* :code:`exact` : true/false whether title matching is exact or fuzzy (optional, default: false)
* :code:`doi` : filter for the DOI of the paper (optional)
* :code:`visibility` : possible values are "ALL_LISTED", "UNLISTED", "FEATURED", "NON_FEATURED", "DELETED" (optional)
* :code:`verified` : true/false for the verified flag of the paper (optional)
* :code:`created_by` : filter for the UUID of the user or service who created this paper (optional)
* :code:`created_at_start` : the oldest timestamp a returned paper can have (optional)
* :code:`created_at_end` : the most recent timestamp a returned paper can have (optional)
* :code:`observatory_id` : filter for the UUID of the observatory that the paper belongs to (optional)
* :code:`organization_id` : filter for the UUID of the organization that the resource belongs to (optional)
* :code:`research_field` : filter for research field id (optional)
* :code:`include_subfields` : true/false whether subfields are included or not (optional, default: false)
* :code:`page` : the page number (optional)
* :code:`size` : number of items per page (optional)
* :code:`sort` : string of key to sort on (optional)
* :code:`desc` : true/false to sort desc (optional)

.. code-block:: python

    ### Get list of papers
    orkg.papers.get(title="covid", visibility="UNLISTED", size=size, research_field="R33")
    >>> (Success)
    [
        {
            'id': 'R187008',
            'title': 'Ontology for Preliminary Detection of COVID-19',
            'research_fields': [{'id': 'R33', 'label': 'Epidemiology'}],
            'identifiers': {'doi': ['']},
            'publication_info': {
                'published_month': None,
                'published_year': None,
                'published_in': {'id': 'R187009', 'label': 'Springer'},
                'url': 'https://link.springer.com/chapter/10.1007/978-981-16-0739-4_34'
            },
            'authors': [
                {'id': None, 'name': 'Poly Sil Sen', 'identifiers': {}, 'homepage': None},
                {'id': None, 'name': ' Shabnam Banerjee', 'identifiers': {}, 'homepage': None},
                {'id': None, 'name': ' Nandini Mukherjee ', 'identifiers': {}, 'homepage': None}
            ],
            'contributions': [{'id': 'R187010', 'label': 'Contribution 1'}],
            'sdgs': [],
            'observatories': ['00000000-0000-0000-0000-000000000000'],
            'organizations': ['00000000-0000-0000-0000-000000000000'],
            'extraction_method': 'UNKNOWN',
            'created_at': '2022-05-22T07:44:59.429738Z',
            'created_by': 'aa79db4d-6762-4eb3-88fe-4db43405970c',
            'verified': False,
            'visibility': 'UNLISTED',
            'modifiable': True
        },
        {
            'id': 'R187054',
            'title': 'COVID19 Ontology for analyzing the Karnataka Private Medical Establishments Data',
            'research_fields': [{'id': 'R33', 'label': 'Epidemiology'}],
            'identifiers': {'doi': ['']},
            'publication_info': {
                'published_month': None,
                'published_year': None,
                'published_in': {'id': 'R187055', 'label': 'BioPortal'},
                'url': 'https://bioportal.bioontology.org/ontologies/COKPME'
            },
            'authors': [{'id': None, 'name': 'Abhilash C B', 'identifiers': {}, 'homepage': None}],
            'contributions': [{'id': 'R187056', 'label': 'Contribution 1'}],
            'sdgs': [],
            'observatories': ['00000000-0000-0000-0000-000000000000'],
            'organizations': ['00000000-0000-0000-0000-000000000000'],
            'extraction_method': 'UNKNOWN',
            'created_at': '2022-05-22T12:29:27.145388Z',
            'created_by': 'aa79db4d-6762-4eb3-88fe-4db43405970c',
            'verified': False,
            'visibility': 'UNLISTED',
            'modifiable': True
        },
    ]

Getting papers by lookup without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also fetch a list of papers using the same search filters as above, but without pagination.  If you don't specify a start and end page, the response will return all papers matching the given parameters in that ORKG instance.

Parameters:

* :code:`start_page` : page to start from, defaults to 0 (optional)
* :code:`end_page` : page to stop at, defaults to -1 meaning non-stop (optional)
* :code:`title` : string of search term that must be contained in the title of the paper (optional)
* :code:`exact` : true/false whether title matching is exact or fuzzy (optional, default: false)
* :code:`doi` : filter for the DOI of the paper (optional)
* :code:`visibility` : possible values are "ALL_LISTED", "UNLISTED", "FEATURED", "NON_FEATURED", "DELETED" (optional)
* :code:`verified` : true/false for the verified flag of the paper (optional)
* :code:`created_by` : filter for the UUID of the user or service who created this paper (optional)
* :code:`created_at_start` : the oldest timestamp a returned paper can have (optional)
* :code:`created_at_end` : the most recent timestamp a returned paper can have (optional)
* :code:`observatory_id` : filter for the UUID of the observatory that the paper belongs to (optional)
* :code:`organization_id` : filter for the UUID of the organization that the resource belongs to (optional)
* :code:`research_field` : filter for research field id (optional)
* :code:`include_subfields` : true/false whether subfields are included or not (optional, default: false)
* :code:`page` : the page number (optional)
* :code:`size` : number of items per page (optional)
* :code:`sort` : string of key to sort on (optional)
* :code:`desc` : true/false to sort desc (optional)

.. code-block:: python

    ### Get list of papers without pagination
    orkg.papers.get_unpaginated(title="covid", start_page=2, end_page=5)
    >>> (Success)
    [
        {
            'id': 'R276025',
            'title': 'WHO Declares COVID-19 a Pandemic',
            'research_fields': [{'id': 'R215741', 'label': 'Requirements Engineering'}],
            'identifiers': {'doi': ['10.23750/abm.v91i1.9397']},
            'publication_info': {
                'published_month': None,
                'published_year': 2020,
                'published_in': {'id': 'R276026', 'label': 'Acta bio-medica : Atenei Parmensis'},
                'url': None
            },
            'authors': [
                {'id': None, 'name': 'D. Cucinotta', 'identifiers': {}, 'homepage': None},
                {'id': None, 'name': 'M. Vanelli', 'identifiers': {}, 'homepage': None}
            ],
            'contributions': [{'id': 'R276027', 'label': 'Contribution 1'}],
            'sdgs': [],
            'observatories': ['00000000-0000-0000-0000-000000000000'],
            'organizations': ['00000000-0000-0000-0000-000000000000'],
            'extraction_method': 'UNKNOWN',
            'created_at': '2023-03-23T11:18:36.14077Z',
            'created_by': '8761632b-f1c6-4e98-83d6-4307464e447a',
            'verified': False,
            'visibility': 'UNLISTED',
            'modifiable': True
        },
        ...
    ]

Getting contributors to papers by ORKG ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can fetch a list of IDs of contributors for a given paper.

Parameter:

* :code:`paper_id` : string of the paper's ORKG ID

.. code-block:: python

    ### Fetch contributors to a paper by orkg id
    orkg.papers.get_contributors(paper_id="R8186")
    >>> (Success)
    [
        '00000000-0000-0000-0000-000000000000',
        '0434e7cc-4e37-4803-b3e9-a4ae2cfc7f26',
        '1ce9b643-32aa-439a-8237-058342cc2b6a'
    ]
