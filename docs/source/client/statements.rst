ORKG Statements
===============
Entities in the ORKG are connected to each other via predicates, the is struct of tiples <subject, predicate, object> is referred to as a statement, in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *statements* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the statements manager directly to do the following:

Getting statement by ID
^^^^^^^^^^^^^^^^^^^^^^^
You can get certain statements given that you know their ID value

.. code-block:: python

    ### Fetch statement by id
    # id: the statement id
    orkg.statements.by_id(id='S5')
    >>> (Success)
    {
       "id":"S5",
       "subject":{
          "id":"R0",
          "label":"Gruber's design of ontologies",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P0",
          "label":"addresses",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"R8",
          "label":"Design of ontologies",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Getting statements
^^^^^^^^^^^^^^^^^^
You can get a list of statements. A variety of parameter can be passed to specify what order you need them in and how many results.

.. code-block:: python

    ### Get all statements
    # all the parameters are optional
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    orkg.statements.get(size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements. A variety of parameter can be passed to specify what order you need them in and how many results without pagination.

.. code-block:: python

    ### Get all statements
    # all the parameters are optional
    # page: the page number (optional)
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    # start_page: page to start from. Defaults to 0 (optional)
    # end_page: page to stop at. Defaults to -1 meaning non-stop (optional)
    orkg.statements.get_unpaginated(size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by subject
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific subject

.. code-block:: python

    ### Get all statements by subject
    # subject_id is the subject to filter on
    # other parameters are optional
    orkg.statements.get_by_subject(subject_id='R0', size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by subject without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific subject without pagination.

.. code-block:: python

    ### Get all statements by subject
    # subject_id is the subject to filter on
    # other parameters are optional
    orkg.statements.get_by_subject_unpaginated(subject_id='R180000', size=30, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S702004",
          "subject":{
             "id":"R180000",
             "label":"Lucian Popa",
             "created_at":"2022-04-25T06:43:17.709163Z",
             "classes": ['Author'],
             "shared":1,
             "created_by":"98a4aa12-a516-44b6-83df-72ea8e3e128f",
             "_class":"resource"
          },
          "predicate":{
             "id":"HAS_ORCID",
             "label":"Has ORCID",
             "created_at":"2019-01-06T15:04:07.692Z",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"L473000",
             "label":"0000-0002-0659-9144",
             "created_at":"2022-04-25T06:43:17.892203Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"literal"
          },
          "created_at":"2022-04-25T06:43:18.780205Z",
          "created_by":"98a4aa12-a516-44b6-83df-72ea8e3e128f"
       },
       ...
    ]


Getting statements by predicate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific predicate

.. code-block:: python

    ### Get all statements by predicate
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_predicate(predicate_id='P0', size=20, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by predicate without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific predicate without pagination.

.. code-block:: python

    ### Get all statements by predicate
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_predicate_unpaginated(predicate_id='S83994', size=20, sort='id', desc=True)
    >>> (Success)
    [
       {
          "id":"S83994",
          "subject":{
             "id":"R26669'",
             "label":"Residual Energy Based Clustering for Energy EfficientWireless Sensor Networks",
             "created_at":"2020-03-30T21:15:58.467+02:00",
             "classes":['Paper'],
             "shared":0,
             "created_by":"07a4c04e-f0ed-407e-a097-b676d5228a40",
             "_class":"resource"
          },
          "predicate":{
             "id":"P26",
             "label":"doi",
             "created_at":"2022-06-30T08:23:10.805977Z",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"L52938",
             "label":"10.1007/978-90-481-3517-2_18",
             "created_at":"2020-03-30T21:15:58.472+02:00",
             "classes":[
             ],
             "shared":1,
             "created_by":"07a4c04e-f0ed-407e-a097-b676d5228a40",
             "_class":"literal"
          },
          "created_at":"2020-03-30T21:15:58.63+02:00",
          "created_by":"07a4c04e-f0ed-407e-a097-b676d5228a40"
       },
       ...
    ]

Getting statements by object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific object

.. code-block:: python

    ### Get all statements by object
    # object_id is the object to filter on
    # other parameters are optional
    orkg.statements.get_by_object(object_id='R8', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by object without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can list statements that have a specific object without pagination.

.. code-block:: python

    ### Get all statements by object
    # object_id is the object to filter on
    # other parameters are optional
    orkg.statements.get_by_object_unpaginated(object_id='R57', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S866521",
          "subject":{
             "id":"R230087",
             "label":"The validation and utility of a quantitative one-step multiplex RT real-time PCR targeting Rotavirus A and Norovirus",
             "created_at":"2022-10-27T15:18:36.592015Z",
             "classes":['Paper'],
             "shared":0,
             "created_by":"c1ec39cf-bf74-4841-82c2-d0657612c7f1",
             "_class":"resource"
          },
          "predicate":{
             "id":"P30",
             "label":"research fieldaddresses",
             "created_at":"2022-06-30T08:20:29.062134Z",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R57",
             "label":"Virology",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":['ResearchField'],
             "shared":382,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2022-10-27T15:21:23.311886Z",
          "created_by":"c1ec39cf-bf74-4841-82c2-d0657612c7f1"
       },
       ...
    ]
Getting statements by subject and predicate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific subject and predicate

.. code-block:: python

    ### Get all statements by subject and predicate
    # subject_id is the subject to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_subject_and_predicate(subject_id='R0', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by subject and predicate without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific subject and predicate without pagination.

.. code-block:: python

    ### Get all statements by subject and predicate
    # subject_id is the subject to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_subject_and_predicate_unpaginated(subject_id='R0', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]


Getting statements by predicate and object
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific predicate and object.

.. code-block:: python

    ### Get all statements by object and predicate
    # object_id is the object to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_object_and_predicate(object_id='R8', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S5",
          "subject":{
             "id":"R0",
             "label":"Gruber's design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "predicate":{
             "id":"P0",
             "label":"addresses",
             "created_at":"2020-05-11T15:06:23.588208+02:00",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R8",
             "label":"Design of ontologies",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":[
             ],
             "shared":1,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000"
       },
       ...
    ]

Getting statements by predicate and object without pagination
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of statements that have a specific predicate and object without pagination.

.. code-block:: python

    ### Get all statements by object and predicate
    # object_id is the object to filter on
    # predicate_id is the predicate to filter on
    # other parameters are optional
    orkg.statements.get_by_object_and_predicate_unpaginated(object_id='R8', predicate_id='P0', size=5, sort='id', desc=False)
    >>> (Success)
    [
       {
          "id":"S504483",
          "subject":{
             "id":"R110711",
             "label":"Antiviral Chromones from the Stem of Cassia siamea",
             "created_at":"2021-05-31T16:48:57.249372+02:00",
             "classes":['Paper'],
             "shared":0,
             "created_by":"a2c4ea2c-1bda-4098-b624-18acd9b55fa1",
             "_class":"resource"
          },
          "predicate":{
             "id":"P30",
             "label":"research field",
             "created_at":"2022-06-30T08:20:29.062134Z",
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"predicate"
          },
          "object":{
             "id":"R57",
             "label":"Virology",
             "created_at":"2019-01-06T15:04:07.692Z",
             "classes":['ResearchField'],
             "shared":382,
             "created_by":"00000000-0000-0000-0000-000000000000",
             "_class":"resource"
          },
          "created_at":"2021-05-31T16:48:57.749018+02:00",
          "created_by":"a2c4ea2c-1bda-4098-b624-18acd9b55fa1"
       },
       ...
    ]



Adding a new statement
^^^^^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new statements in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added statements will be credited to your user.*

.. code-block:: python

    ### Add a statement
    # subject_id: the id of the resource subject
    # predicate_id: the id of the predicate
    # object_id: the id of the resource, or literal to be placed at the object position
    orkg.statements.add(subject_id='R1', predicate_id='P2', object_id='L3')
    >>> (Success)
    {
       "id":"S32131",
       "subject":{
          "id":"R1",
          "label":"Some resource",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P2",
          "label":"some predicate",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"L3",
          "label":"some literal",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"literal"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Updating an existing statement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing statements in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Update a statement
    # not available on Labs yet
    # id: is the id of the statement to edit
    # other parameters are optional
    orkg.statements.update(id='S32131', subject_id='R1', predicate_id='P3', object_id='L3')
    >>> (Success)
    {
       "id":"S32131",
       "subject":{
          "id":"R1",
          "label":"Some resource",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       "predicate":{
          "id":"P3",
          "label":"some other predicate!",
          "created_at":"2020-05-11T15:06:23.588208+02:00",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"predicate"
       },
       "object":{
          "id":"L3",
          "label":"some literal",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"literal"
       },
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000"
    }

Check if a statement exist
^^^^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of statements before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if statement exists
    # id: the id of the statement
    # returns a bool
    orkg.statements.exists(id='S1')
    >>> True
