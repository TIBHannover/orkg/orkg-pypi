ORKG Comparisons
=========================
The comparisons client is a special client of the ORKG that handles publishing comparisons. For that reason it needs to be connected to the `simcomp` service of the Open Research Knowledge Graph (ORKG).

If you instantiate the ORKG client using a web address or host environment name (``Hosts.PRODUCTION``, ``Hosts.SANDBOX``, or ``Hosts.INCUBATING``), the corresponding `simcomp_host` is automatically assigned.

However, if you are running the ORKG locally then you need to specify `simcomp_host`. See :ref:`Host <host>` for details.

Publishing a comparison in the ORKG involves several steps. First, a resource in the graph is which represents the comparison and contains relations to all metadata of the comparison. Second, the comparison itself is computed using the SimComp service. Last, the comparison with its configuration is stored within the SimComp service for persistence.

You can access the comparisons client directly to do the following:


Publish comparisons
^^^^^^^^^^^^^^^^^^^^^^^^^
The python package offers an abstraction over the steps needed to publish a comparison. Only one method is needed to publish a comparison, which is the `publish` method.

.. code-block:: python

    ### title is required
    ### You need either contribution_ids (live data) or data (precomputed data)
    orkg.comparisons.publish(
        contribution_ids=['R8197', 'R8198', 'R8199'],
        title='Comparison of three contributions',
        description='This is a comparison of three contributions',
    )
    >>> (Success)
    {
       "id":"R15780",
       "label":"Comparison of three contributions",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
            "Comparison"
       ],
       "shared":1,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

    # If you have the data already you can PASS the data directly and NOT compute the live comparison via the contribution_ids
    orkg.comparisons.publish(
        data=comparison_data,
        title='Comparison of two contributions',
        description='This is a comparison of two contributions'
    )
     >>> (Success)
     {
       "id":"R15781",
       "label":"Comparison of three contributions",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
            "Comparison"
       ],
       "shared":1,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

The publish method allows for greater flexibility with other parameters, such as:
- `title`: The title of the comparison
- `description`: The description of the comparison
- `reference`: The reference of the comparison
- `contribution_ids`: The contribution ids to compare
- `data`: The comparison data
- `comparison_type`: The comparison method to use (possible values are of the ComparisonType enum)
- `predicate`: The predicate to view on the comparison
- `transposed`: Whether to transpose the comparison

Get comparisons by research field ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of comparisons from within a particular research field.  You may optionally include comparisons from subfields as well.  If the response is an empty list, it means the research field has no comparisons (or that the given ID does not correspond to a resource with the 'ResearchField' class label).

.. code-block:: python

    ### Fetch comparisons by research field id
    # research_field_id: the id of the research field
    # include_subfields: True/False whether to include comparisons from subfields, default is False (optional)
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.comparisons.in_research_field('R132', include_subfields=True, size=3)
    >>> (Success)
    [
       {
          'id': 'R8340',
          'label': 'Semi-supervised author name disambiguation',
          'classes': ['Comparison']
       },
       {
          'id': 'R8341',
          'label': 'Semi-supervised author name disambiguation',
          'classes': ['Comparison']
      },
      {
          'id': 'R8342',
          'label': 'Ontologies for describing scholarly articles',
          'classes': ['Comparison']
      }
    ]
