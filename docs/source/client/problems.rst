ORKG Problems
=============
Entities in the ORKG can be of type *Problem*, which means they represent a research problem that papers address or talk about.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the problems manager directly to do the following:

Get problems by research field ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a list of problems that are addressed by papers inside a particular research field.  You may optionally include problems from subfields as well.

.. code-block:: python

    ### Fetch problem by research field id
    # research_field_id: the id of the research field
    # include_subfields: True/False whether to include problems from subfields, default is False (optional)
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.problems.in_research_field('R199', sort='problem.label', desc=True)
    >>> (Success)
    [
       {
          "problem":{
             "id":"R333090",
             "label":"Hyperglycemia in Type 2 Diabetes",
             "classes":[
                "Problem"
             ],
             "shared":1,
             "featured":false,
             "unlisted":false,
             "verified":false,
             "observatory_id":"00000000-0000-0000-0000-000000000000",
             "organization_id":"00000000-0000-0000-0000-000000000000",
             "created_at":"2023-09-12T12:16:50.245989+02:00",
             "created_by":"e98d2312-c2a7-4420-96b7-9788aeac26d2",
             "extraction_method":"UNKNOWN",
             "_class":"resource",
             "formatted_label":null
          },
          "papers":1
       }
    ]

If you include problems from subfields, the response is a list of resource objects without the ``problems`` and ``papers`` keys.

.. code-block:: python

    orkg.problems.in_research_field('R132', include_subfields=True, sort='id')
    >>> (Success)
    [
       {
          'id': 'R1032',
          'label': 'Structured descriptions of research contributions',
          'classes': ['Problem']
       },
       {
          'id': 'R1033',
          'label': 'Scholarly communications representation',
          'classes': ['Problem']
      }
    ]
