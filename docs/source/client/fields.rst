ORKG Research Fields
=============================
This is a special client of the ORKG that handle all the operations to manipulate the research fields

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the research fields manager directly to do the following:

Getting research fields containing benchmarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get any research fields that contain papers with benchmarks in them.

.. code-block:: python

    ### Get research fields with benchmarks
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.fields.with_benchmarks()
    >>> (Success)
    {
       [
            {
                'id': 'R1',
                'label': 'Computer Sciences'
            },
            {
                'id': 'R2',
                'label': 'Life Science'
            },
       ]
    }

    # You can also use the pagination parameters with the function
    orkg.fields.with_benchmarks(size=1, page=2)
    >>> (Success)
    {
       [
            {
                'id': 'R2',
                'label': 'Life Science'
            }
       ]
    }

Getting the subfields of a research field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get the subfield(s) immediately below a given research field.  This method returns multiple fields if the given field has multiple immediate children at the same level in the hierarchy; it does **not** return multiple levels of subfields.  For each subfield, the ``child_count`` key gives the number of subfields immediately under it.  If the response is an empty list, it means the given research field has no subfields.

.. code-block:: python

    ### Get subfields of a given research field
    # field_id: id of the research field
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.fields.get_subfields(field_id="R136180", size=2, sort='resource.id')
    >>> (Success)
    [
       {
            'resource':
                {
                    'id': 'R396',
                    'label': 'French and Francophone Language and Literature',
                    'classes': ['ResearchField']
                }
            'child_count': 2
       },
       {
            'resource':
                {
                    'id': 'R460',
                    'label': 'Spanish and Portuguese Language and Literature',
                    'classes': ['ResearchField']
                }
            'child_count': 4
       }
    ]

Getting the superfields of a research field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get the superfield(s) immediately above a given research field.  This method returns multiple fields if the given subfield has multiple immediate parents at the same level in the hierarchy; it does **not** return multiple levels of superfields.  If you want the complete hierarchy, please use the ``get_hierarchy`` method (see below).  If the response is an empty list, it means the given research field has no superfields.

.. code-block:: python

    ### Get superfields of a given research field
    # field_id: id of the research field
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.fields.get_superfields(field_id="R137546")
    >>> (Success)
    [
        {
            'id': 'R137646',
            'label': 'Production Technology',
            'classes': ['ResearchField']
        }
    ]

Getting the root of a research field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get the root research field of a given research field.  A root research field is at the highest level of the field taxonomy, with no superfields above it.  If the response is an empty list, it means the given field is itself the root.

.. code-block:: python

    ### Get the root of a given research field
    # field_id: id of the research field
    orkg.fields.get_root(field_id="R136088")
    >>> (Success)
    [
        {
            'id': 'R11',
            'label': 'Science',
            'classes': ['ResearchField']
        }
    ]

Getting all research field roots
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get a list of all the root research fields in your current ORKG instance.  A root research field is at the highest level of the field taxonomy, with no superfields above it.

.. code-block:: python

    ### Get all research field roots
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.fields.get_all_roots()
    >>> (Success)
    [
        {
            'id': 'R11',
            'label': 'Science',
            'classes': ['ResearchField']
        },
        {
            'id': 'R161726',
            'label': 'Digital Humanities',
            'classes': ['ResearchField']
        },
        {
            'id': 'R75674',
            'label': 'Seafood science and technology',
            'classes': ['ResearchField']
        }
    ]

Getting the hierarchy above a research field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get a list of all research fields in the hierarchy above and including the given research field. If the list has only one element, it means the provided field is itself the root.  Note that the list elements may not be in hierarchical order, but you can recover the hierarchy using the ``parent_ids`` key in each element.

.. code-block:: python

    ### Get the hierarchy above a given research field
    # field_id: id of the research field
    # page: the page number (optional)
    # size: number of items per page (optional)
    # sort: key to sort on (optional)
    # desc: true/false to sort desc (optional)
    orkg.fields.get_hierarchy(field_id="R397")
    >>> (Success)
    [
        {
            'resource':
                {
                    'id': 'R11',
                    'label': 'Science',
                    'classes': ['ResearchField']
                },
            'parent_ids': []
        },
        {
            'resource':
                {
                    'id': 'R136180',
                    'label': 'Literary Studies',
                    'classes': ['ResearchField']
                },
            'parent_ids': ['R375']
        }
    ]

Getting statistics about a research field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get the number of papers, comparisons, and their sum within a research field, optionally including all subfields under the given research field.

.. code-block:: python

    ### Get statistics about a given research field
    # field_id: id of the research field
    # include_subfields: True/False whether to include subfields in the statistics, default is False (optional)
    orkg.fields.get_stats(field_id="R194", include_subfields=True)
    >>> (Success)
    {
        'id': 'R194',
        'papers': 2102,
        'comparisons': 194,
        'total': 2296
    }
