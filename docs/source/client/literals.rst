ORKG Literals
=============
Entities in the ORKG can be of type *Literal*, this mean they represents the ground values (strings, numbers, dates), in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *literals* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the literals manager directly to do the following:

Getting literal by ID
^^^^^^^^^^^^^^^^^^^^^^^
You can get certain literals given that you know their ID value

.. code-block:: python

    ### Fetch literal by id
    orkg.literals.by_id(id='L1')
    >>> (Success)
    {
       "id":"L1",
       "label":"ORKG is the best project ever",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"literal"
    }

Getting literals by lookup
^^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup literals by the value of the literal.

.. code-block:: python

    ### Get all literals (Not paginated)
    # all the parameters are optional
    orkg.literals.get_all(q='ORKG', exact=False)
    >>> (Success)
    [
       {
          "id":"L1",
          "label":"ORKG is the best project ever",
          "created_at":"2019-01-06T15:04:07.692Z",
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"literal"
       },
       ...
    ]

Adding a new literal
^^^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new literals in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added literals will be credited to your user.*

.. code-block:: python

    ### Add a literal
    # ID is optional
    orkg.literals.add(label='Science rocks!')
    >>> (Success)
    {
       "id":"L1234",
       "label":"Science rocks!",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"literal"
    }

New literal with datatype
^^^^^^^^^^^^^^^^^^^^^^^^^
You can also assign the datatype when creating a literal

*Note: if you have you credentials entered in the ORKG instance creation all newly added literals will be credited to your user.*

.. code-block:: python

    ### Add a literal
    # ID is optional
    orkg.literals.add(label='ORKG rocks!', datatype='xsd:string')
    >>> (Success)
    {
       "id":"L3321",
       "label":"ORKG rocks!",
       "datatype":"xsd:string",
       "created_at":"2022-08-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"literal"
    }

Updating an existing literal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing literal in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Update a literal
    # not available on Labs yet
    orkg.literals.update(id='L1', label='Coco')
    >>> (Success)
    {
       "id":"L1",
       "label":"Coco",
       "datatype":"xsd:string",
       "created_at":"2019-01-06T15:04:07.692Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"literal"
    }

    ### You can also update the datatype of a literal
    # not available on Labs yet
    orkg.literals.update(id='L1', label='20', datatype='xsd:int')
    >>> (Success)
    {
       "id":"L1",
       "label":"20",
       "datatype":"xsd:int",
       "created_at":"2022-07-06T15:04:07.693Z",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"literal"
    }

Check if a literal exists
^^^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of literals before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if literal exists
    # id: the id of the literal
    # returns a bool
    orkg.literals.exists(id='L1')
    >>> True
