Installation and Usage
======================
The wrapper is pretty lightweight and doesn't need any requirements.

Installation
^^^^^^^^^^^^
The package can be installed using pip

.. code-block:: bash

    $ pip install orkg

Or the source can be cloned from:

.. code-block:: bash

    $ git clone https://gitlab.com/TIBHannover/orkg/orkg-pypi.git

Usage
^^^^^
In order to use the package in your python code, you just need to import it and instantiate an instance of the base class to use it.

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG

The package can be used to connect to any instance of the ORKG local or remote. The host parameter may be specified with an address or environment name (see below). Optionally you can provide the credentials to authenticate your requests. If host is not specified, it defaults to `sandbox <https://sandbox.orkg.org/>`_.

.. _host-label:
Host
**********************

Three different host environments are available:

* `Production <https://orkg.org/>`_: The most stable version of the ORKG.  Use this host to access the current version of the graph and add persistent data.

.. code-block:: python

    from orkg import ORKG, Hosts # import base class from package

    orkg = ORKG(host=Hosts.PRODUCTION) # create the connector to the ORKG

* `Sandbox <https://sandbox.orkg.org/>`_: The ORKG playground!  It has the same features that exist on production at any given time. Use this host to experiment with the ORKG or Python package features without adding data to the main graph.  Data added to sandbox is periodically deleted, and this version of the ORKG may not contain all of the data found in the production graph.

.. code-block:: python

    from orkg import ORKG, Hosts # import base class from package

    orkg = ORKG(host=Hosts.SANDBOX) # create the connector to the ORKG

* `Incubating <https://incubating.orkg.org/>`_: The ORKG version with the newest features being tested.  Since features are still under development, this version may break or not function as expected. Data added to incubating is periodically deleted, and this version of the ORKG may not contain all of the data found in the production graph.
.. code-block:: python

    from orkg import ORKG, Hosts # import base class from package

    orkg = ORKG(host=Hosts.INCUBATING) # create the connector to the ORKG

* Other hosts: If you're working on a local or other on-site instances of the ORKG, you will need to set the simcomp host manually.
.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="http://127.0.0.1:8080", simcomp_host="http://127.0.0.1:5000", creds=('email-address', 'password')) # create the connector to the ORKG


.. note::  Your credentials may differ between the different host environments.

Logging
**********************

You may optionally include the parameter ``logging_level`` when you instantiate your ORKG instance in order to print logging messages to the terminal.  Specify the severity threshold from which you wish to activate logging; the default level is ``'WARNING'``.

* ``'DEBUG'`` or ``10``: detailed diagnostic information with insight into the flow of the program and the values of variables.
* ``'INFO'`` or ``20``: general information about the execution of the program.
* ``'WARNING'`` or ``30``: information when something unexpected happens during the execution of a program. (default)
* ``'ERROR'`` or ``40``: information when any functionalities are not operating correctly.

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", logging_level='DEBUG') # create the connector to the ORKG
